#include <systemc.h>
#include <string>
#include "CLA_Adder.hpp"
#include "stim_adder.hpp"

int sc_main (int ac, char** argv)
{
	sc_signal<bool> A[ADD_SIZE], B[ADD_SIZE], S[ADD_SIZE];
	sc_signal<bool> op, ovf;
	sc_clock testClk("TestClock", 10, SC_NS, 0.5);
	sc_clock intClk("InternalClock", 1, SC_NS, 0.5);
	
	StimAdder<ADD_SIZE> stimAdder("adder_stim");
	stimAdder.clk(testClk);
	stimAdder.opcode(op);
	for (int i=0; i<ADD_SIZE; i++)
	{
		stimAdder.A[i](A[i]);
		stimAdder.B[i](B[i]);	
	}	
	
	CLA_Adder adder8("cla_adder_dut");
	adder8.clk(intClk);
	adder8.in_carry(op);
	for (int i=0; i<ADD_SIZE; i++)
	{
		adder8.in_A(A[i]);
		adder8.in_B(B[i]);
		adder8.out_S(S[i]);
	}
	adder8.out_carry(ovf);
	
	sc_start(80, SC_NS);	
			
	cout << endl << "S = ";
	for (int k=0; k<ADD_SIZE; k++)
	{
		cout << S[k];
	}
	cout << " with carry_out equal to " << ovf << endl;
			  	
	return 0;
}