#include <systemc.h>
#include "lil_denorm.hpp"
#include "stim_lil_denorm.hpp"
#include "check_lil_denorm.hpp"

using namespace std;

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<32> > A_in, B_in, A_out, B_out;
	sc_signal<bool> ld_eq, ld_gt, ld_zero;
	sc_clock testClk("TestClock", 10, SC_NS, 0.5);
	sc_clock dutClk("DUT_Clock", 1, SC_NS, 0.5);
		
	StimLilDenorm stim("lil_denorm_stim");
	stim.clk(testClk);
	stim.A(A_in);
	stim.B(B_in);	
	
	LilDenorm dut("lil_denorm_dut");
	dut.clk(dutClk);
	dut.in_A(A_in);
	dut.in_B(B_in);
	dut.out_A(A_out);	
	dut.out_B(B_out);
	dut.exp_eq(ld_eq);
	dut.exp_gt(ld_gt);
	dut.man_ovf(ld_zero);

	CheckLilDenorm check("lil_denorm_check");
	check.A_in(A_in);
	check.B_in(B_in);
	check.A_out(A_out);	
	check.B_out(B_out);
	check.eq(ld_eq);
	check.gt(ld_gt);
	check.zero(ld_zero);
	
	sc_start(100, SC_NS);	
	
	sc_stop();	
	  	
	return 0;
}