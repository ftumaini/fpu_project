#include <systemc.h>
#include <string>
#include "adder.hpp"
#include "stim_adder.hpp"

int sc_main (int ac, char** argv)
{
	std::string a1, a2;
	sc_signal<bool> A[ADD_SIZE], B[ADD_SIZE], S[ADD_SIZE];
	sc_signal<bool> op, ovf;
	sc_clock testClk("TestClock", 10, SC_NS, 0.5);
	
	StimAdder<ADD_SIZE> stimAdder("adder_stim");
	stimAdder.clk(testClk);
	stimAdder.opcode(op);
	for (int i=0; i<ADD_SIZE; i++)
	{
		stimAdder.A[i](A[i]);
		stimAdder.B[i](B[i]);	
	}	
	
	Adder<ADD_SIZE> adder("adder_dut");
	adder.opcode(op);
	for (int i=0; i<ADD_SIZE; i++)
	{
		adder.in_A[i](A[i]);
		adder.in_B[i](B[i]);
		adder.res[i](S[i]);
	}
	adder.overflow(ovf);
		
	if (ac > 1) {
		a1 = argv[1];
		a2 = argv[1];
		if (ac > 2) {
			a2 = argv[2];
		}
		for (int j=0; j<ADD_SIZE; j++)
		{
			A[j] = (a1[j] == '0') ? false : true;
			B[j] = (a2[j] == '0') ? false : true;
		}
	}		
	
	sc_start(80, SC_NS);	
			
	cout << endl << "S = ";
	for (int k=0; k<ADD_SIZE; k++)
	{
		cout << S[k];
	}
	cout << " with overflow equal to " << ovf << endl;
			  	
	return 0;
}