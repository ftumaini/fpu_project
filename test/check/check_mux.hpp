#ifndef CHECK_MUX_HPP
#define CHECK_MUX_HPP

#include <systemc.h>

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
class CheckMux : public sc_module 
{
	public:
	CheckMux(sc_module_name cm_name);

	sc_in<bool> clk;
	sc_in<sc_bv<D_SIZE*D_NUM> > gen_data;
	sc_in<sc_bv<SEL_SIZE> > gen_sel;
	sc_in<sc_bv<D_SIZE> > out_data;

	void check();
	
	private:
	sc_time t0;
};

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
CheckMux<D_SIZE, D_NUM, SEL_SIZE>::CheckMux(sc_module_name cm_name)
:t0(sc_time_stamp())
{
	SC_HAS_PROCESS(CheckMux);

	SC_THREAD(check);
	sensitive << clk.pos();
}

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
void CheckMux<D_SIZE, D_NUM, SEL_SIZE>::check()
{
	while (true) {
		wait();
		t0 = sc_time_stamp();
		cout << "@ t = " << t0.to_string() << endl;; 
		cout << "  inputs:" << endl;
		cout << gen_data << endl;
		cout << "  [number " << gen_sel.read().to_uint() << " is selected]" << endl;
		cout << "  outputs:" << endl;
		cout << out_data << endl << endl;
	}
}

#endif