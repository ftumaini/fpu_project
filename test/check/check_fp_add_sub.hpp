#ifndef CHECK_FP_ADD_SUB_HPP
#define CHECK_FP_ADD_SUB_HPP

#include <systemc.h>
#include "SinglePrecisionDecoder.hpp"

class CheckFPAddSub : public sc_module
{
public:
	CheckFPAddSub(sc_module_name cfpas_name);

	sc_in<bool> clk, clk_dut;
	sc_in<sc_bv<1> > opcode;
	sc_in<sc_bv<32> > A, B;
	sc_in<sc_bv<32> > S;
	
private:
	SinglePrecisionDecoder SPD;
	void GetFPAddSubInout();
	void CheckFPAddSubGen();
	void PrintFPAddSubClk();

	sc_bv<32> temp_A, temp_B, temp_S;	
};

CheckFPAddSub::CheckFPAddSub(sc_module_name cfpas_name)
:SPD(-127)
{
	SC_HAS_PROCESS(CheckFPAddSub);

	SC_METHOD(PrintFPAddSubClk);
	sensitive << clk_dut;
	dont_initialize();

	SC_METHOD(GetFPAddSubInout);
	sensitive << S;
	dont_initialize();

	SC_THREAD(CheckFPAddSubGen);
	sensitive << clk;
	dont_initialize();
}

void CheckFPAddSub::GetFPAddSubInout()
{
	temp_A = A.read();
	temp_B = B.read();
	temp_S = S.read();
}

void CheckFPAddSub::PrintFPAddSubClk()
{
	if (clk_dut.posedge()) {
		//cout << "MAIN CLK @ t =" << sc_time_stamp() << endl;
	}
}

void CheckFPAddSub::CheckFPAddSubGen()
{
	string sA, sB, sS;
	cout.precision(23);

	while (true) {
		wait();
		
		cout << "@ t = ";
		cout << sc_time_stamp() << endl;; 
		
		sA = SPD.binaryToFP32(temp_A);
		sB = SPD.binaryToFP32(temp_B);
		sS = SPD.binaryToFP32(temp_S);

		cout << "  inputs [op=" << opcode <<  "]:" << endl;
		cout << A.read()[31] << "|" << A.read().range(30,23) << "|"; 
		cout << A.read().range(22,0) << " = " << sA << endl;
		cout << B.read()[31] << "|" << B.read().range(30,23) << "|"; 
		cout << B.read().range(22,0) << " = "  << sB << endl;
		cout << "  outputs:" << endl;
		cout << S.read()[31] << "|" << S.read().range(30,23) << "|"; 
		cout << S.read().range(22,0) << " = "  << sS << endl << endl;
	}
}

#endif