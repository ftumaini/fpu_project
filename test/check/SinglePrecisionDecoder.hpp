#ifndef SINGLE_PREC_ENC_HPP
#define SINGLE_PREC_ENC_HPP

/*
 * This class contanis methods for handling IEEE 754 encoded 32-bit words;
 * the binary representation format is the following:
 * 
 * 31                                 0
 * +-+--------+-----------------------+
 * |S|EXPONENT|       MANTISSA        |
 * |I|(8 bits)|       (22 bits)       |
 * |G|        |                       |
 * |N|        |                       |
 * +-+--------+-----------------------+
 *
 * A given number is so represented: (-1)^SIGN . MANTISSA * 2^(EXPONENT+BIAS)
 *
 * The bias factor is used to keep EXPONENT field unsigned (from 0 to 255) and
 * so its value is fixed at +127 for 32-bit floating point numbers.
 *
 * EXPONENT and MANTISSA bit sequences divide numbers in 5 categories:
 * 1) zeros (EXPONENT and MANTISSA all set to zero)
 * 2) denormalized numbers (EXPONENT = 0, non zero MANTISSA), in range ]-1,+1[
 * 3) normalized numbers (non zero EXPONENT and any MANTISSA)
 * 4) infinites (EXPONENT = 255, MANTISSA = 0)
 * 5) not-a-number, or NaN (EXPONENT = 255, non zero MANTISSA)
 */

#include <string>
#include <sstream>
#include <systemc.h>

using namespace std;

class SinglePrecisionDecoder
{
	public:
		SinglePrecisionDecoder();
		SinglePrecisionDecoder(int bias);

		string binaryToFP32(const sc_bv<32>& bs);

	private:
		int exp_bias; // un-biasing factor
};

SinglePrecisionDecoder::SinglePrecisionDecoder()
:exp_bias(-127)
{
}


SinglePrecisionDecoder::SinglePrecisionDecoder(int bias)
:exp_bias(bias)
{
}

string SinglePrecisionDecoder::binaryToFP32(const sc_bv<32>& bs)
{
	int exp = 0;
	unsigned mant_size = 0; // number of "trailing zeros" (right to the mantissa)
	ostringstream exp_oss, mant_oss;
	double mant = 0.0;
	double weight = 1.0;
	string temp;

	mant_oss.precision(23);
	
	exp = exp_bias;
	exp += bs.range(30,23).to_uint();
	exp_oss << exp;

	while (!(bs[mant_size])) {
		if (mant_size == 23) {
			break;
		}
		mant_size++;
	}

	//cout << endl << mant_size << endl;

	for (int i = 0; i < 23 - mant_size; i++) {
		weight = weight / 2.0;
		mant = (bs[22 - i]) ? (mant + weight) : mant;
	}

	if (exp == -127) {
		if (mant == 0.0) {	// the number is zero ('+0' or '-0')...
			temp = "0";
		}
		else {			// ...or is not normalized
			temp = (bs[31]) ? "-" : "+";
			mant_oss << fixed << mant;
			temp = mant_oss.str();
		}
		return temp;
	}
	
	if (exp == 128) {
		if (mant == 0.0) {	// the number is an infinite...
			temp = (bs[31]) ? "-" : "+";
			temp += "Inf";
		}
		else {			// ...or NaN (Not-a-Number)
			temp = "NaN";
		}
		return temp;
	}
	
	// the number is normalized
	mant += 1.0;
	mant_oss << fixed << mant;
	
	temp = (bs[31]) ? "-" : "+";
	temp += mant_oss.str();
	temp += "x2^";
	temp += exp_oss.str();	

	return temp;
}

#endif