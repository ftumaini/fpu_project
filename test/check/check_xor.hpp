#ifndef CHECK_XOR_HPP
#define CHECK_XOR_HPP

#include <systemc.h>

template <unsigned D_SIZE, unsigned D_NUM>
class CheckXor : public sc_module 
{
	public:
	CheckXor(sc_module_name cx_name);

	sc_in<bool> clk;
	sc_port<sc_signal_in_if<sc_bv<D_SIZE> >, D_NUM> gen_data;
	sc_in<sc_bv<D_SIZE> > out_data;

	void check();
	
	private:
	sc_time t0;
};

template <unsigned D_SIZE, unsigned D_NUM>
CheckXor<D_SIZE, D_NUM>::CheckXor(sc_module_name cx_name)
:t0(sc_time_stamp())
{
	SC_HAS_PROCESS(CheckXor);

	SC_THREAD(check);
	sensitive << clk.pos();
}

template <unsigned D_SIZE, unsigned D_NUM>
void CheckXor<D_SIZE, D_NUM>::check()
{
	while (true) {
		wait();
		t0 = sc_time_stamp();
		cout << "@ t = " << t0.to_string() << endl;; 
		cout << "  input :" << endl;
		for (unsigned i=0; i<D_NUM; i++) {
			cout << gen_data[i]->read() << endl;
		}
		cout << "  output:" << endl;
		cout << out_data << endl << endl;
	}
}

#endif