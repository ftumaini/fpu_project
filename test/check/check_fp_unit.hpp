#ifndef CHECK_FP_UNIT_HPP
#define CHECK_FP_UNIT_HPP

#include <systemc.h>
#include "SinglePrecisionDecoder.hpp"

class CheckFPUnit : public sc_module
{
public:
	CheckFPUnit(sc_module_name cfp_unit_name);

	sc_in<bool> clk;
	sc_in<sc_bv<2> > operation;
	sc_in<sc_bv<32> > A, B;
	sc_in<sc_bv<32> > Res;
	
private:
	SinglePrecisionDecoder SPD;
	void GetFPUnitInout();
	void CheckFPUnitGen();

	sc_bv<32> temp_A, temp_B, temp_Res;
	sc_bv<2> temp_operation;	
};

CheckFPUnit::CheckFPUnit(sc_module_name cfp_unit_name)
:SPD(-127)
{
	SC_HAS_PROCESS(CheckFPUnit);

	SC_METHOD(GetFPUnitInout);
	sensitive << Res;
	dont_initialize();

	SC_THREAD(CheckFPUnitGen);
	sensitive << clk.pos();
	dont_initialize();
}

void CheckFPUnit::GetFPUnitInout()
{
	temp_A = A.read();
	temp_B = B.read();
	temp_Res = Res.read();
	temp_operation = operation.read();
}

void CheckFPUnit::CheckFPUnitGen()
{
	string sA, sB, sRes;
	cout.precision(23);

	while (true) {
		wait();
		
		cout << "@ t = ";
		cout << sc_time_stamp() << endl;; 
		
		sA = SPD.binaryToFP32(temp_A);
		sB = SPD.binaryToFP32(temp_B);
		sRes = SPD.binaryToFP32(temp_Res);

		cout << "  input(s):" << endl;
		cout << "operation code is " << temp_operation << endl;
		cout << A.read()[31] << "|" << A.read().range(30,23) << "|"; 
		cout << A.read().range(22,0) << " = " << sA << endl;
		cout << B.read()[31] << "|" << B.read().range(30,23) << "|"; 
		cout << B.read().range(22,0) << " = "  << sB << endl;
		cout << "  output:" << endl;
		cout << Res.read()[31] << "|" << Res.read().range(30,23) << "|"; 
		cout << Res.read().range(22,0) << " = "  << sRes << endl << endl;
	}
}

#endif