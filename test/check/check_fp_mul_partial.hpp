#ifndef CHECK_FP_MUL_PARTIAL_HPP
#define CHECK_FP_MUL_PARTIAL_HPP

#include <iomanip>
#include <systemc.h>

using namespace std;

class CheckFPMulPartial : public sc_module
{
public:
	CheckFPMulPartial(sc_module_name cfpas_name);

	sc_in<bool> clk;
	sc_in<sc_bv<23> > A, B;
	sc_in<sc_bv<23*23> > S;
	
private:
	void GetFPMulPartialInout();
	void CheckFPMulPartialGen();
	
	sc_bv<32> temp_A, temp_B, temp_S;	
};

CheckFPMulPartial::CheckFPMulPartial(sc_module_name cfpas_name)
{
	SC_HAS_PROCESS(CheckFPMulPartial);

	SC_METHOD(GetFPMulPartialInout);
	sensitive << S;
	dont_initialize();

	SC_THREAD(CheckFPMulPartialGen);
	sensitive << clk;
	dont_initialize();
}

void CheckFPMulPartial::GetFPMulPartialInout()
{
	temp_A = A.read();
	temp_B = B.read();
	temp_S = S.read();
}

void CheckFPMulPartial::CheckFPMulPartialGen()
{
	while (true) {
		wait();
		
		cout << "@ t = ";
		cout << sc_time_stamp() << endl;; 
		
		cout << "  inputs:" << endl;
		cout << setw(46) << A.read() << endl;
		cout << setw(46) << B.read() << endl;
		cout << "  output (stream):" << endl;
		for (unsigned i=0; i<23; i++) {
			cout << setw(46-i) << S.read().range(23*(i+1)-1,23*i) << endl;
		}
		cout << endl;
	}
}

#endif