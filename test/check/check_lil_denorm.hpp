#ifndef CHECK_LIL_DENORM_HPP
#define CHECK_LIL_DENORM_HPP

#include <sstream>
#include <string>

using namespace std;

class CheckLilDenorm : public sc_module 
{
public:
	CheckLilDenorm(sc_module_name cld_name);

	sc_in<sc_bv<32> > A_in, B_in;
	sc_in<sc_bv<32> > A_out, B_out;
	sc_in<bool> eq, gt, zero;
	
	string binaryToFP32(sc_bv<32> bs);
private:
	void check();
};

CheckLilDenorm::CheckLilDenorm(sc_module_name cld_name)
{
	SC_HAS_PROCESS(CheckLilDenorm);

	SC_THREAD(check);
	sensitive << A_out;
	sensitive << B_out;
	sensitive << eq;
	sensitive << gt;
	sensitive << zero;
	dont_initialize();
}


void CheckLilDenorm::check()
{
	cout.precision(24);
	cout << "@ t = " << sc_time_stamp() << endl;
	cout << "  Inputs are:" << endl;
	cout << "    A_in  = " << binaryToFP32(A_in.read()) << endl;
	cout << "    B_in  = " << binaryToFP32(B_in.read()) << endl;
	cout << "  Outputs are:" << endl;
	cout << "    A_out = " << binaryToFP32(A_out.read()) << endl;
	cout << "    B_out = " << binaryToFP32(B_out.read()) << endl;
	cout << "    [exp_A";
	if (eq) {
		cout << " = ";
	}
	else if (gt) {
		cout << " > ";
	}
	else {
		cout << " < ";
	}
	cout << "exp_B and mantissa shift overflow is set to " << zero << "]" << endl;	
}

/*
 * Convert a binary floating point 32-bit number in 
 * scientific (base 2) notation string: 
 *    <sign>1.<mantissa>x2^<signed_exponent>
 */
string CheckLilDenorm::binaryToFP32(sc_bv<32> bs)
{
	int exp = -127; // un-biasing factor
	unsigned mant_size = 0; // number of "trailing zeros" (right to the mantissa)
	ostringstream exp_oss, mant_oss;
	unsigned mant = 0;
	unsigned weight = 1;
	string temp;

	exp += bs.range(30,23).to_uint();
	exp_oss << exp;

	while ((!(bs[mant_size])) || (mant_size == 22)) {
		mant_size++;
	}
	for (int i = mant_size; i < 23; i++) {
		mant = (bs[i]) ? (mant + weight) : mant;
		weight = weight * 2;
	}
	mant = ((exp != -127) || (exp != 128)) ? mant + weight : mant;
	mant_oss << mant;
	
	temp = (bs[31]) ? "-" : "+";
	if (exp != -127) { // the number is not normalized
		temp += mant_oss.str();
		temp += "x2^";
		temp += exp_oss.str();
	}
	else {
		temp = "0";
	}	

	return temp;
}

#endif