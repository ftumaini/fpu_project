#ifndef CHECK_FP_MUL_HPP
#define CHECK_FP_MUL_HPP

#include <systemc.h>
#include "SinglePrecisionDecoder.hpp"

class CheckFPMul : public sc_module
{
public:
	CheckFPMul(sc_module_name cfp_mul_name);

	sc_in<bool> clk;
	sc_in<sc_bv<32> > A, B;
	sc_in<sc_bv<32> > P;
	
private:
	SinglePrecisionDecoder SPD;
	void GetFPMulInout();
	void CheckFPMulGen();

	sc_bv<32> temp_A, temp_B, temp_P;	
};

CheckFPMul::CheckFPMul(sc_module_name cfp_mul_name)
:SPD(-127)
{
	SC_HAS_PROCESS(CheckFPMul);

	SC_METHOD(GetFPMulInout);
	sensitive << P;
	dont_initialize();

	SC_THREAD(CheckFPMulGen);
	sensitive << clk.pos();
	dont_initialize();
}

void CheckFPMul::GetFPMulInout()
{
	temp_A = A.read();
	temp_B = B.read();
	temp_P = P.read();
}

void CheckFPMul::CheckFPMulGen()
{
	string sA, sB, sP;
	cout.precision(23);

	while (true) {
		wait();
		
		cout << "@ t = ";
		cout << sc_time_stamp() << endl;; 
		
		sA = SPD.binaryToFP32(temp_A);
		sB = SPD.binaryToFP32(temp_B);
		sP = SPD.binaryToFP32(temp_P);

		cout << "  input(s):" << endl;
		cout << A.read()[31] << "|" << A.read().range(30,23) << "|"; 
		cout << A.read().range(22,0) << " = " << sA << endl;
		cout << B.read()[31] << "|" << B.read().range(30,23) << "|"; 
		cout << B.read().range(22,0) << " = "  << sB << endl;
		cout << "  output:" << endl;
		cout << P.read()[31] << "|" << P.read().range(30,23) << "|"; 
		cout << P.read().range(22,0) << " = "  << sP << endl << endl;
	}
}

#endif