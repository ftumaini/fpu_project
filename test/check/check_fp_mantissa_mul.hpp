#ifndef CHECK_FP_MUL_MANTISSA_HPP
#define CHECK_FP_MUL_MANTISSA_HPP

#include <iomanip>
#include <systemc.h>

using namespace std;

class CheckFPMulMantissa : public sc_module
{
public:
	CheckFPMulMantissa(sc_module_name cfpas_name);

	sc_in<bool> clk;
	sc_in<sc_bv<23> > A, B;
	sc_in<sc_bv<23+23> > S;
	
private:
	void CheckNewInputs();
	void GetFPMulMantissaInout();
	void CheckFPMulMantissaGen();
	
	sc_bv<32> temp_A, temp_B, temp_S;	
};

CheckFPMulMantissa::CheckFPMulMantissa(sc_module_name cfpas_name)
{
	SC_HAS_PROCESS(CheckFPMulMantissa);

	SC_METHOD(GetFPMulMantissaInout);
	sensitive << S;
	dont_initialize();

	SC_THREAD(CheckNewInputs);
	sensitive << A;
	sensitive << B;

	SC_THREAD(CheckFPMulMantissaGen);
	sensitive << clk;
	dont_initialize();
}

void CheckFPMulMantissa::GetFPMulMantissaInout()
{
	temp_A = A.read();
	temp_B = B.read();
	temp_S = S.read();
}

void CheckFPMulMantissa::CheckNewInputs()
{
	while (true) {
		wait();
		
		cout << "@ t = ";
		cout << sc_time_stamp() << endl;; 
		
		cout << "  new inputs are coming:" << endl;
		cout << setw(46) << A.read() << endl;
		cout << setw(46) << B.read() << endl;
	}
}
void CheckFPMulMantissa::CheckFPMulMantissaGen()
{
	while (true) {
		wait();
		
		cout << "@ t = ";
		cout << sc_time_stamp() << endl;; 
		
		cout << "  inputs:" << endl;
		cout << setw(46) << A.read() << endl;
		cout << setw(46) << B.read() << endl;
		cout << "  output (stream):" << endl;
		cout << setw(46) << S.read() << endl;
		cout << endl;
	}
}

#endif