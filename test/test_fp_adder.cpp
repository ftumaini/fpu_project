#include <systemc.h>
#include "FloatingPointAdder.hpp"
#include "stim_fp_add_sub.hpp"
#include "check_fp_add_sub.hpp"

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<32> > A_in, B_in, S;
	sc_signal<sc_bv<1> > op;
	sc_clock stimClk("StimClock", 10, SC_NS, 0.5);
	sc_clock dutClk("DUT_Clock", 1, SC_NS, 0.5);
	sc_clock testClk("TestClock", 30, SC_NS, 0.5);
		
	StimFPAddSub stimulus("fp_addsub_stim");
	stimulus.clk(stimClk);
	stimulus.A(A_in);
	stimulus.B(B_in);
	stimulus.operation(op);	
	
	FloatingPointAdder dut("fp_adder_dut");
	dut.clk(dutClk);
	dut.opcode(op);
	dut.in_A(A_in);
	dut.in_B(B_in);
	dut.out_S(S);

	CheckFPAddSub checker("fp_addsub_check");
	checker.clk(testClk);
	checker.clk_dut(dutClk);
	checker.opcode(op);
	checker.A(A_in);
	checker.B(B_in);
	checker.S(S);

	sc_start(1, SC_SEC);	
		  	
	return 0;
}