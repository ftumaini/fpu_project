#include <systemc.h>
#include "MuxGate.hpp"
#include "stim_mux.hpp"
#include "check_mux.hpp"

#define MUX_DATA_SIZE 23
#define MUX_DATA_WIDTH 2
#define MUX_SEL_SIZE 1 

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<MUX_DATA_SIZE*MUX_DATA_WIDTH> > A;
	sc_signal<sc_bv<MUX_DATA_SIZE> > B;
	sc_signal<sc_bv<MUX_SEL_SIZE> > S;
	
	sc_clock testClk("TestClock", 2, SC_NS, 0.5);
	
	StimMux<MUX_DATA_SIZE, MUX_DATA_WIDTH, MUX_SEL_SIZE> stimMux("multiplexer_stim");
	stimMux.clk(testClk);
	stimMux.gen_sel(S);
	stimMux.gen_data(A);
		
	MuxGate<MUX_DATA_SIZE, MUX_DATA_WIDTH, MUX_SEL_SIZE> mux("mux_dut");
	mux.data_in(A);
	mux.sel(S);
	mux.data_out(B);	
	
	CheckMux<MUX_DATA_SIZE, MUX_DATA_WIDTH, MUX_SEL_SIZE> checkMux("multiplexer_checker");
	checkMux.clk(testClk);
	checkMux.gen_sel(S);
	checkMux.gen_data(A);
	checkMux.out_data(B);
	

	sc_start(80, SC_NS);				
			  	
	return 0;
}