#include <systemc.h>
#include <string>
#include "adder.hpp"

int sc_main (int ac, char** argv)
{
	std::string a1, a2, carry;
	FullAdder f_adder("f_adder_dut");
	bool zero(0), one(1);
	sc_signal<bool> A, B, S, Cin, Cout;
				
	if (ac > 1) {
		a1 = argv[1];
		a2 = argv[2];
	}
	if (ac > 2) {
		a2 = argv[2];
	}
	if (ac > 3) {
		carry = argv[3];
	}
	A = (a1[0] == '0') ? zero : one;
	B = (a2[0] == '0') ? zero : one;
	Cin = (carry[0] == '0') ? zero : one;
	
	f_adder.A(A);
	f_adder.B(B);
	f_adder.S(S);
	f_adder.Cin(Cin);
	f_adder.Cout(Cout);
	
	sc_start();
		
	cout << endl;
	cout << "A = " << A << endl;
	cout << "B = " << B << endl;
	cout << "[input carry = " << Cin << "]" << endl;
	cout << "S = A + B = " << S << " with carry equal to " << Cout << endl;
			  	
	return 0;
}