#ifndef STIM_ADDER_HPP
#define STIM_ADDER_HPP

#include "RandomBitGen.hpp"

class StimLilDenorm : public sc_module 
{
public:
	StimLilDenorm(sc_module_name sld_name);

	sc_in<bool> clk;
	sc_out<sc_bv<32> > A, B;

	unsigned wait_time;
private:
	void stimLilDenormGen();
	void write_data(sc_bv<32> dA, sc_bv<32> dB);
	
	RandomBitGen<32> Rg;
};

StimLilDenorm::StimLilDenorm(sc_module_name sld_name)
:Rg(1), wait_time(1)
{
	SC_HAS_PROCESS(StimLilDenorm);

	SC_THREAD(stimLilDenormGen);
	sensitive << clk.pos();
	dont_initialize();
}


void StimLilDenorm::stimLilDenormGen()
{
	Rg.Init();
	wait(1, SC_NS);
	write_data(Rg.Rand(),Rg.Rand());
	wait(11, SC_NS);
}

void StimLilDenorm::write_data(sc_bv<32> dA, sc_bv<32> dB)
{
	A.write(dA);
	B.write(dB);
}

#endif