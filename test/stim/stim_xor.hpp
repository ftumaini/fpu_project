#ifndef STIM_XOR_HPP
#define STIM_XOR_HPP

#include <systemc.h>
#include <cstdlib>

template <unsigned D_SIZE, unsigned D_NUM>
class StimXor : public sc_module 
{
	public:
	StimXor(sc_module_name sx_name);

	sc_in<bool> clk;
	sc_port<sc_signal_inout_if<sc_bv<D_SIZE> >, D_NUM> gen_data;
	
	void StimXorGen();
	void write_data(sc_bv<D_SIZE*D_NUM> buffer);
	
	unsigned wait_time;
	private:
		void randomize(sc_bv<D_SIZE*D_NUM>& d);
};

template <unsigned D_SIZE, unsigned D_NUM>
StimXor<D_SIZE, D_NUM>::StimXor(sc_module_name sx_name)
:wait_time(2)
{
	SC_HAS_PROCESS(StimXor);

	SC_THREAD(StimXorGen);
	sensitive << clk.pos();
}

template <unsigned D_SIZE, unsigned D_NUM>
void StimXor<D_SIZE, D_NUM>::StimXorGen()
{
	sc_bv<D_SIZE*D_NUM> data;
	
	wait(wait_time, SC_NS);
	randomize(data);
	write_data(data);
	wait(wait_time, SC_NS);
	randomize(data);
	write_data(data);
	wait(wait_time, SC_NS);
	
	sc_stop();
}

template <unsigned D_SIZE, unsigned D_NUM>
void StimXor<D_SIZE, D_NUM>::write_data(sc_bv<D_SIZE*D_NUM> buffer)
{
	for (unsigned i=0; i<D_NUM; i++) {
		gen_data[i]->write(buffer.range(i*D_SIZE,(i+1)*D_SIZE-1));
	}
}

template <unsigned D_SIZE, unsigned D_NUM>
void StimXor<D_SIZE, D_NUM>::randomize(sc_bv<D_SIZE*D_NUM>& d)
{
	for (unsigned i=0; i<D_SIZE*D_NUM; i++) {
		d[i] = rand() % 2;
	}
}

#endif