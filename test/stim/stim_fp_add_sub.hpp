#ifndef STIM_FP_ADD_SUB_HPP
#define STIM_FP_ADD_SUB_HPP

#include <systemc.h>
#include "RandomBitGen.hpp"

class StimFPAddSub : public sc_module
{
public:
	StimFPAddSub(sc_module_name sfpas_name);

	sc_in<bool> clk;
	
	sc_out<sc_bv<1> > operation;
	sc_out<sc_bv<32> > A;
	sc_out<sc_bv<32> > B;
	
private:
	unsigned wait_time;

	void StimFPAddSubGen();
	void write_data(sc_bv<32> dA, sc_bv<32> dB, bool opcode = false);

	RandomBitGen<32> Rg;
	sc_bv<32> zero;
};

StimFPAddSub::StimFPAddSub(sc_module_name sfpas_name)
:Rg(1), wait_time(1)
{
	SC_HAS_PROCESS(StimFPAddSub);

	SC_THREAD(StimFPAddSubGen);
	sensitive << clk.pos();
	dont_initialize();
}


void StimFPAddSub::StimFPAddSubGen()
{
	Rg.Init();
	wait(wait_time, SC_NS);
	write_data((Rg.Rand().range(1,0),"1111111",Rg.Rand().range(22,0)),(Rg.Rand().range(1,0),"1111111",Rg.Rand().range(22,0)));
	//write_data(Rg.Rand(),Rg.Rand());
	//wait(15, SC_NS);
	//write_data((Rg.Rand().range(0,0),"0000000",Rg.Rand().range(2,0),"000000000000000000000"),(Rg.Rand().range(0,0),"0000000",Rg.Rand().range(2,0),"000000000000000000000"));
	wait(150, SC_NS);
	
	sc_stop();	
}

void StimFPAddSub::write_data(sc_bv<32> dA, sc_bv<32> dB, bool opcode)
{
	A.write(dA);
	B.write(dB);
	operation[0]->write(opcode);
}

#endif