#ifndef STIM_MUX_HPP
#define STIM_MUX_HPP

#include <systemc.h>

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
class StimMux : public sc_module 
{
	public:
	StimMux(sc_module_name sm_name);

	sc_in<bool> clk;
	sc_out<sc_bv<D_SIZE*D_NUM> > gen_data;
	sc_out<sc_bv<SEL_SIZE> > gen_sel;

	void StimMuxGen();
	void write_data(sc_bv<D_SIZE*D_NUM> buffer);
	void write_sel(sc_bv<SEL_SIZE> buffer);

	sc_time t0;
	unsigned wait_time;
};

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
StimMux<D_SIZE, D_NUM, SEL_SIZE>::StimMux(sc_module_name sm_name)
:wait_time(2), t0(sc_time_stamp())
{
	SC_HAS_PROCESS(StimMux);

	SC_THREAD(StimMuxGen);
	sensitive << clk.pos();
}

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
void StimMux<D_SIZE, D_NUM, SEL_SIZE>::StimMuxGen()
{
	sc_bv<D_SIZE*D_NUM> data;
	sc_bv<SEL_SIZE> selection;

	data = "1000000000010000000010010000010000000110000001";
	cout << data.range(0,22) << " | " << data.range(23,45) << endl;
	wait(wait_time, SC_NS);
	write_data(data);
	write_sel(selection);
	wait(10*wait_time, SC_NS);
	selection = "1";
	write_sel(selection);
	wait(10*wait_time, SC_NS);
	
	sc_stop();
}

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
void StimMux<D_SIZE, D_NUM, SEL_SIZE>::write_data(sc_bv<D_SIZE*D_NUM> buffer)
{
	t0 = sc_time_stamp();
	cout << t0.to_string() << endl;
	gen_data.write(buffer);
}

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
void StimMux<D_SIZE, D_NUM, SEL_SIZE>::write_sel(sc_bv<SEL_SIZE> buffer)
{
	t0 = sc_time_stamp();
	cout << t0.to_string() << endl;
	gen_sel.write(buffer);
}
#endif