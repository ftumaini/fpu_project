#ifndef STIM_FP_UNIT_HPP
#define STIM_FP_UNIT_HPP

#include <systemc.h>
#include "RandomBitGen.hpp"

class StimFPUnit : public sc_module
{
public:
	StimFPUnit(sc_module_name sfpm_name);

	sc_in<bool> clk;
	sc_out<sc_bv<2> > operation;
	sc_out<sc_bv<32> > A;
	sc_out<sc_bv<32> > B;
	
private:
	unsigned wait_time;

	void StimFPUnitGen();
	void write_data(sc_bv<32> dA, sc_bv<32> dB, sc_bv<2> op);

	RandomBitGen<32> Rg;
	sc_bv<32> zero;
};

StimFPUnit::StimFPUnit(sc_module_name sfpm_name)
:Rg(1), wait_time(1)
{
	SC_HAS_PROCESS(StimFPUnit);

	SC_THREAD(StimFPUnitGen);
	sensitive << clk.pos();
	dont_initialize();
}


void StimFPUnit::StimFPUnitGen()
{
	Rg.Init();
	wait(wait_time, SC_NS);
	write_data(Rg.Rand(),Rg.Rand(),Rg.Rand().range(1,0));
	wait(150, SC_NS);
	
	sc_stop();	
}

void StimFPUnit::write_data(sc_bv<32> dA, sc_bv<32> dB, sc_bv<2> op)
{
	A.write(dA);
	B.write(dB);
	operation.write(op);
}

#endif