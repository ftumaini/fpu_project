#ifndef STIM_FP_MUL_MANT_HPP
#define STIM_FP_MUL_MANT_HPP

#include <systemc.h>
#include "RandomBitGen.hpp"

class StimFPMantMul : public sc_module
{
public:
	StimFPMantMul(sc_module_name sfpm_name);

	sc_in<bool> clk;
	
	sc_out<sc_bv<23> > A;
	sc_out<sc_bv<23> > B;
	
private:
	unsigned wait_time;

	void StimFPMulGen();
	void write_data(sc_bv<32> dA, sc_bv<32> dB);

	RandomBitGen<23> Rg;
	sc_bv<32> zero;
};

StimFPMantMul::StimFPMantMul(sc_module_name sfpm_name)
:Rg(1), wait_time(1)
{
	SC_HAS_PROCESS(StimFPMantMul);

	SC_THREAD(StimFPMulGen);
	sensitive << clk.pos();
	dont_initialize();
}


void StimFPMantMul::StimFPMulGen()
{
	//Rg.Init();
	wait(wait_time, SC_NS);
	write_data(("111",Rg.Rand().range(19,0)),("101",Rg.Rand().range(19,0)));
	wait(150, SC_NS);
	
	sc_stop();	
}

void StimFPMantMul::write_data(sc_bv<32> dA, sc_bv<32> dB)
{
	A.write(dA);
	B.write(dB);
}

#endif