#ifndef RANDOM_BIT_GEN_HPP
#define RANDOM_BIT_GEN_HPP

#include <systemc.h>
#include <cstdlib>
#include <ctime>

template <unsigned R_SIZE>
class RandomBitGen 
{
	public:
	RandomBitGen(int seed = 1);
	
	void Reset();
	void Init();
	sc_bv<R_SIZE> Rand(unsigned r_left = 0, unsigned r_right = R_SIZE-1, bool isSigned = true);
	
	private:
	int default_seed;
};

template <unsigned R_SIZE>
RandomBitGen<R_SIZE>::RandomBitGen(int seed)
{
	default_seed = seed;
}

template <unsigned R_SIZE>
void RandomBitGen<R_SIZE>::Reset()
{
	srand(time(default_seed));
}

template <unsigned R_SIZE>
void RandomBitGen<R_SIZE>::Init()
{
	srand(time(NULL));
}

template <unsigned R_SIZE>
sc_bv<R_SIZE> RandomBitGen<R_SIZE>::Rand(unsigned r_left, unsigned r_right, bool isSigned)
{
	sc_bv<R_SIZE> r_temp;

	if ((r_right - r_left) >= (R_SIZE - 1)) {
		for (unsigned i=0; i<R_SIZE-1; i++) {
			r_temp[i] = rand() % 2;
		}
	}
	else {
		for (unsigned i=r_left; i<r_right; i++) {
			r_temp[i] = rand() % 2;		// random mantissa
		}
		for (unsigned i=r_right; i<R_SIZE-3; i++) {
			r_temp[i] = false;		// exponent bits all set to zero
		}
	        r_temp[R_SIZE-2] = true;		// (except for the exponent MSB)
	}

	if (!(isSigned)) {
		r_temp[R_SIZE-1] = false;		// always positive sign
	}
	else {
		r_temp[R_SIZE-1] = rand() % 2;		// random sign
	}

	return r_temp;
}

#endif