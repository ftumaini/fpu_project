#ifndef STIM_ADDER_HPP
#define STIM_ADDER_HPP

template <unsigned add_size>
class StimAdder : public sc_module 
{
	public:
	StimAdder(sc_module_name sa_name);

	sc_in<bool> clk;
	sc_out<bool> A[add_size], B[add_size], opcode;

	sc_bv<add_size> buffer_A, buffer_B;
	sc_signal<bool> operation;

	void stimAdderGen();
	void write_data(sc_bv<add_size> buffer_A, sc_bv<add_size> buffer_B, bool op = false);

	sc_time t0;
	unsigned wait_time;
};

template <unsigned add_size>
StimAdder<add_size>::StimAdder(sc_module_name sa_name)
:wait_time(2), t0(sc_time_stamp())
{
	SC_HAS_PROCESS(StimAdder);

	SC_THREAD(stimAdderGen);
	sensitive << clk.pos();
}

template <unsigned add_size>
void StimAdder<add_size>::stimAdderGen()
{
	wait(wait_time, SC_NS);
	write_data("00000001","00000001");
	wait(10*wait_time, SC_NS);
	write_data("00000110","00000011");
	wait(7*wait_time, SC_NS);
	write_data("00001000","00011001");
	wait(13*wait_time, SC_NS);
	sc_stop();
}

template <unsigned add_size>
void StimAdder<add_size>::write_data(sc_bv<add_size> buffer_A, sc_bv<add_size> buffer_B, bool op)
{
	t0 = sc_time_stamp();
	cout << t0.to_string() << endl;
	for (int i=0; i<add_size; i++) {
		A[i].write(buffer_A[add_size-1-i]);
		B[i].write(buffer_B[add_size-1-i]);
	}
	opcode.write(op);
}
#endif