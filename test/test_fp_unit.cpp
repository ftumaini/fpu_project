#include <systemc.h>
#include "FloatingPointUnit.hpp"
#include "stim_fp_unit.hpp"
#include "check_fp_unit.hpp"

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<32> > A_in, B_in;
	sc_signal<sc_bv<32> > Result;
	sc_clock stimClk("StimClock", 150, SC_NS, 0.5);
	sc_clock dutClk("DUTClock", 1, SC_NS, 0.5);
	sc_clock testClk("TestClock", 135, SC_NS, 0.5);
	sc_signal<sc_bv<2> > op;
		
	StimFPUnit stimulus("fp_unit_stim");
	stimulus.clk(stimClk);
	stimulus.operation(op);
	stimulus.A(A_in);
	stimulus.B(B_in);

	FloatingPointUnit dut("fp_mul_dut");
	dut.clk(dutClk);
	dut.opcode(op);
	dut.fp_in_A(A_in);
	dut.fp_in_B(B_in);
	dut.fp_out(Result);

	CheckFPUnit checker("fp_unit_check");
	checker.clk(testClk);
	checker.operation(op);
	checker.A(A_in);
	checker.B(B_in);
	checker.Res(Result);

	sc_start(1, SC_SEC);	
		  	
	return 0;
}