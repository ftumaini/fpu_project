#include <systemc.h>
#include "MantissaMult.hpp"
#include "stim_fp_part_mul.hpp"
#include "check_fp_mantissa_mul.hpp"

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<23> > A_in, B_in;
	sc_signal<sc_bv<23+23> > P_out;
	sc_clock stimClk("StimClock", 10, SC_NS, 0.5);
	sc_clock dutClk("DUTClock", 1, SC_NS, 0.5);
	sc_clock testClk("TestClock", 5, SC_NS, 0.5);
		
	StimFPMantMul stimulus("fp_mul_stim");
	stimulus.clk(stimClk);
	stimulus.A(A_in);
	stimulus.B(B_in);

	MantMultiplier dut("fp_mmul_dut");
	dut.Clk(dutClk);
	dut.Ma(A_in);
	dut.Mb(B_in);
	dut.Prod(P_out);

	CheckFPMulMantissa checker("fp_mpart_check");
	checker.clk(testClk);
	checker.A(A_in);
	checker.B(B_in);
	checker.S(P_out);

	sc_start(1, SC_SEC);	
		  	
	return 0;
}