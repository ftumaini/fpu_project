#include <systemc.h>
#include "comp.hpp"

#define COMP_SIZE 8

int sc_main (int ac, char** argv)
{
	cout << "operand size is " << COMP_SIZE << endl;	
	
	Comp<COMP_SIZE> comp("comp_dut");
	sc_signal<sc_bv<COMP_SIZE> > A, B;
	sc_signal<bool> is_eq, is_gt;
	comp.in_A(A);
	comp.in_B(B);
	comp.eq(is_eq);
	comp.gt(is_gt);
	
	if (ac > 1) {
		B.write(argv[1]);
		A.write(argv[1]);
	}
		if (ac > 2) {
		B.write(argv[2]);
	}
	sc_start();
		
	cout << endl << "A = " << A << endl;
	cout << "B = " << B << endl;
	
	if (is_eq) {
		cout << "The inputs are equal!" << endl;
	}
	else {
		if (is_gt) {
			cout << "A is greater than B!" << endl;
		}
		else {
			cout << "B is greater than A!" << endl;
		}
	} 	
	return 0;
}