#include <systemc.h>
#include "PartialMult.hpp"
#include "stim_fp_part_mul.hpp"
#include "check_fp_mul_partial.hpp"

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<23> > A_in, B_in;
	sc_signal<sc_bv<23*23> > S_out;
	sc_clock stimClk("StimClock", 10, SC_NS, 0.5);
	sc_clock testClk("TestClock", 30, SC_NS, 0.5);
		
	StimFPMantMul stimulus("fp_mul_stim");
	stimulus.clk(stimClk);
	stimulus.A(A_in);
	stimulus.B(B_in);

	PartialMultiplier dut("fp_pmul_dut");
	dut.Fa(A_in);
	dut.Fb(B_in);
	dut.Pr(S_out);

	CheckFPMulPartial checker("fp_mpart_check");
	checker.clk(testClk);
	checker.A(A_in);
	checker.B(B_in);
	checker.S(S_out);

	sc_start(1, SC_SEC);	
		  	
	return 0;
}