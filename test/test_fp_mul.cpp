#include <systemc.h>
#include "FloatingPointMultiplier.hpp"
#include "stim_fp_add_sub.hpp"
#include "check_fp_mul.hpp"

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<32> > A_in, B_in;
	sc_signal<sc_bv<32> > P_out;
	sc_clock stimClk("StimClock", 150, SC_NS, 0.5);
	sc_clock dutClk("DUTClock", 1, SC_NS, 0.5);
	sc_clock testClk("TestClock", 134, SC_NS, 0.5);
	sc_signal<sc_bv<1> > op;
		
	StimFPAddSub stimulus("fp_mul_stim");
	stimulus.clk(stimClk);
	stimulus.operation(op);
	stimulus.A(A_in);
	stimulus.B(B_in);

	FloatingPointMultiplier dut("fp_mul_dut");
	dut.clk(dutClk);
	dut.in_A(A_in);
	dut.in_B(B_in);
	dut.out_P(P_out);

	CheckFPMul checker("fp_mul_check");
	checker.clk(testClk);
	checker.A(A_in);
	checker.B(B_in);
	checker.P(P_out);

	sc_start(1, SC_SEC);	
		  	
	return 0;
}