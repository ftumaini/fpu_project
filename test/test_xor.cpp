#include <systemc.h>
#include "XorGate.hpp"
#include "stim_xor.hpp"
#include "check_xor.hpp"

#define XOR_DATA_SIZE 23
#define XOR_DATA_WIDTH 2

int sc_main (int ac, char** argv)
{
	sc_signal<sc_bv<XOR_DATA_SIZE> > A[XOR_DATA_WIDTH];
	sc_signal<sc_bv<XOR_DATA_SIZE> > B;
	
	sc_clock testClk("TestClock", 2, SC_NS, 0.5);
	
	StimXor<XOR_DATA_SIZE, XOR_DATA_WIDTH> stimXOR("exor_stim");
	stimXOR.clk(testClk);
	for (unsigned i=0; i<XOR_DATA_WIDTH; i++) {
		stimXOR.gen_data(A[i]);
	}
		
	XorGate<XOR_DATA_SIZE, XOR_DATA_WIDTH> XOR("XOR_dut");
	for (unsigned i=0; i<XOR_DATA_WIDTH; i++) {
		XOR.data_in(A[i]);
	}
	XOR.data_out(B);	
	
	CheckXor<XOR_DATA_SIZE, XOR_DATA_WIDTH> checkXOR("exor_checker");
	checkXOR.clk(testClk);
	for (unsigned i=0; i<XOR_DATA_WIDTH; i++) {
		checkXOR.gen_data(A[i]);
	}
	checkXOR.out_data(B);

	sc_start(80, SC_NS);				
			  	
	return 0;
}