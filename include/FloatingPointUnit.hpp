#ifndef FP_UNIT_HPP
#define FP_UNIT_HPP

/*
 * Floating point unit module for performing addition, subtraction and multiply operations.
 * The total amount of clock periods needed to complete a single operation
 * is 67 (134 clock transitions)
 */

#include "FloatingPointAdder.hpp"
#include "FloatingPointMultiplier.hpp"

#define DATA_WIDTH 32
#define MANT_SIZE 23

class FloatingPointUnit : public sc_module
{
	public:
		FloatingPointUnit(sc_module_name fp_unit_name);
		
		sc_in<bool> clk;
		sc_in<sc_bv<2> > opcode; // 00 = add, 01 = sub, 10 = mul, 11 = not_implemented
		sc_in<sc_bv<DATA_WIDTH> > fp_in_A, fp_in_B;
		sc_out<sc_bv<DATA_WIDTH> > fp_out;
		
	private:
		/* this sc-method propagate inputs to the desired submodule */
		void operandsFetch();

		/* this sc-method get result from the correct module */
		void resultWrite();
		
		FloatingPointAdder FPA;
		sc_signal<sc_bv<1> > add_or_sub;
		sc_signal<sc_bv<DATA_WIDTH> > add_in_A, add_in_B, add_out_res;

		FloatingPointMultiplier FPM;
		sc_signal<sc_bv<DATA_WIDTH> > mul_in_A, mul_in_B, mul_out_mul;
};

#endif