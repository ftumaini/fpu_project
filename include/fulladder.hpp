#ifndef FULLADDER_HPP
#define FULLADDER_HPP

class FullAdder : public sc_module
{
	public:
		FullAdder(sc_module_name fa_name);
		
		sc_in<bool> A;
		sc_in<bool> B;
		sc_in<bool> Cin;
		sc_out<bool> S;
		sc_port<sc_signal_out_if<bool>, 1, SC_ZERO_OR_MORE_BOUND> Cout;

		/* port added for allow using this block in carry-lookahead implementations */
		sc_port<sc_signal_out_if<bool>, 1, SC_ZERO_OR_MORE_BOUND> Gen;
		sc_port<sc_signal_out_if<bool>, 1, SC_ZERO_OR_MORE_BOUND> Pro;
	private:
		sc_event fa_ready, fa_done;

		bool temp_S, temp_Cout, debugMode;
		bool temp_G, temp_P;

		void full_addition();
		void inputs_read();
		void debug_print();
};

#endif