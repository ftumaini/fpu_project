#ifndef LIL_DENORMALIZER_HPP
#define LIL_DENORMALIZER_HPP

#include "CLA_Adder.hpp"
#include "comp.hpp"

class LilDenorm : public sc_module
{
	public:
		LilDenorm(sc_module_name ld_name);

		sc_in<bool> clk;
		sc_in<sc_bv<32> > in_A, in_B;
		sc_out<sc_bv<32> > out_A, out_B;
		sc_out<bool> exp_eq;			// '1' if the 2 input exponents are equal, '0' elsewhere
		sc_out<bool> exp_gt;			// '1' if in_A has larger exponent than in_B, '0' elsewhere
		sc_out<bool> man_ovf;			// '1' if one of the mantissas shift exceed limit dimension, '0' elsewhere

		/* threads */
		void internal_loop();

		/* methods */
		void ld_enable();
		void denormalization();
		void find_minor();
		void exp_offset();
		void propagate_results();

		int boolVectorToInt(sc_bv<8> bv);
		void binaryOneIncrement(sc_bv<8>& bw);
		void binaryOneDecrement(sc_bv<8>& bw);
        private:
		sc_event read_event, write_event, denorm_event, add_event;

		bool zero_flag, enable, debugMode;
		unsigned mant_size, clk_pnum;
		int shamt;

		sc_signal<bool> AeqB, AgtB, op, ovf;
		sc_signal<bool> operand_A[8], operand_B[8], mant_shift[8];
		sc_bv<32> input_A, input_B, output_A, output_B;
		sc_signal<sc_bv<8> > in_comp_A, in_comp_B;

		CLA_Adder exp_adder;
		Comp<8> exp_comp;
};

#endif