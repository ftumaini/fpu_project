#ifndef MANT_MUL_HPP
#define MANT_MUL_HPP

#include "CLA_Adder.hpp"
#include "PartialMult.hpp"

#define F_SIZE 23

using namespace std;

class MantMultiplier : public sc_module
{
	public:
		MantMultiplier(sc_module_name mant_m_name);
		
		sc_in<bool> Clk;
		sc_in<sc_bv<F_SIZE> > Ma;
		sc_in<sc_bv<F_SIZE> > Mb;
		sc_out<sc_bv<2*F_SIZE> > Prod;
		
	private:
		bool enable, debugMode;
		unsigned bufferPos;
		int clk_pnum;
		sc_event productComplete;

		PartialMultiplier pm_Core;
		sc_signal<sc_bv<F_SIZE*F_SIZE> > pm_buffer;
		sc_bv<2*F_SIZE> buffer_pout;
		
		sc_signal<bool> pm_cin[3], pm_cout[3];
		sc_signal<bool> pm_fact_A[F_SIZE+1], pm_fact_B[F_SIZE+1];
		sc_signal<bool> pm_prod[F_SIZE+1];
		sc_vector<CLA_Adder> pm_Adder;
	
		void inner_loop();

		void writeProduct();
		void inputsMul();
		
		void carryUpdate(unsigned index);
		void partialProductsDes(unsigned stageId);
};

#endif