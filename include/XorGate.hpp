#ifndef XOR_GATE_HPP
#define XOR_GATE_HPP

#include <systemc.h>
#include <vector>

template <unsigned D_SIZE, unsigned D_NUM>
class XorGate : public sc_module {
	public:
		XorGate(sc_module_name xor_name);

		sc_port<sc_signal_in_if<sc_bv<D_SIZE> >, D_NUM> data_in;
		sc_out<sc_bv<D_SIZE> > data_out;

	private:
		void combine_output();
};

template <unsigned D_SIZE, unsigned D_NUM>
XorGate<D_SIZE, D_NUM>::XorGate(sc_module_name xor_name)
{
	SC_HAS_PROCESS(XorGate);

	SC_METHOD(combine_output);
	sensitive << data_in;
}

template <unsigned D_SIZE, unsigned D_NUM>
void XorGate<D_SIZE, D_NUM>::combine_output()
{
	std::vector<sc_bv<D_SIZE> > in_bv;
	sc_bv<D_SIZE> temp_bv;

	for (unsigned i=0; i<D_NUM; i++) {
		in_bv.push_back(data_in[i]->read());
		temp_bv = temp_bv ^ in_bv[i];
	}

	data_out.write(temp_bv);
}

#endif