#ifndef CLA_ADDER_HPP
#define CLA_ADDER_HPP

#include "fulladder.hpp"

class CLA_Adder : public sc_module {
	public:
		CLA_Adder(sc_module_name cla_name);

		sc_in<bool> clk;
		
		sc_in<bool> in_carry;
		sc_port<sc_signal_in_if<bool>, 8> in_A;
		sc_port<sc_signal_in_if<bool>, 8> in_B;
		sc_port<sc_signal_out_if<bool>, 8> out_S;
		sc_out<bool> out_carry;

	private:
		sc_event start_read, carry_prop, read_event, write_event;
	
		void add_enable();		

		void elaborate_result();
		void read_inputs();
		void write_result();
		void carry_update();

		void inner_loop();

		void report();

		bool enable, debugMode;
		unsigned counter_clk;
		
		sc_vector<FullAdder> fav;
		sc_vector<sc_signal<bool> > fav_Cin, fav_Cout;
		sc_vector<sc_signal<bool> > fav_Gen;
		sc_vector<sc_signal<bool> > fav_Pro;
		
		sc_signal<bool> operand_A[8], operand_B[8];
		sc_signal<bool> result[8];
		sc_signal<bool> carry, P0, P1, G0, G1;
};

#endif