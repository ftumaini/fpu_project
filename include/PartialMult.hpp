/*
 * Combinatorial block for performing partial multiplication product for an
 * F_SIZE x F_SIZE operation; result is provided in a unique bitstream
 */

#ifndef PART_MUL_HPP
#define PART_MUL_HPP

#define F_SIZE 23

class PartialMultiplier : public sc_module
{
	public:
		PartialMultiplier(sc_module_name pm_name);
		
		sc_in<sc_bv<F_SIZE> > Fa;
		sc_in<sc_bv<F_SIZE> > Fb;
		sc_port<sc_signal_inout_if<sc_bv<F_SIZE*F_SIZE> > > Pr;
		
	private:
		void perform_multi();

		sc_bv<F_SIZE*F_SIZE> p_buffer;
};

#endif