/*
* A simple equality/inequality comparator with 2 binary inputs;
* the outputs are respectively '1' if the two numbers are equal 
* or if the first one (A) is greater than the second one (B)
*/

#ifndef COMP_HPP
#define COMP_HPP

template <int comp_size>
class Comp : public sc_module {
	public:
		Comp(sc_module_name mod_name);
		
		sc_in<sc_bv<comp_size> > in_A;
		sc_in<sc_bv<comp_size> > in_B;
		sc_out<bool> eq;
		sc_out<bool> gt;

	private:
		sc_bv<comp_size> operand_A, operand_B;
		void equality();
		void unequality();	
};

// default constructor 
template <int comp_size>
Comp<comp_size>::Comp(sc_module_name mod_name)
	: sc_module(mod_name)
{
	SC_HAS_PROCESS(Comp);

	SC_METHOD(equality);
	sensitive << in_A << in_B;
	SC_METHOD(unequality);
	sensitive << in_A << in_B;
}

template <int comp_size>
void Comp<comp_size>::equality()
{
	operand_A = in_A->read();
	operand_B = in_B->read();
	
	eq->write((operand_A == operand_B) ? 1 : 0);
	//cout << ((operand_A == operand_B) ? 1 : 0);
}

template <int comp_size>
void Comp<comp_size>::unequality()
{
	operand_A = in_A->read();
	operand_B = in_B->read();
	
	sc_dt::sc_logic_value_t A, B;
	bool X = true;
	A = operand_A.get_bit(comp_size-1);
	B = operand_B.get_bit(comp_size-1);
	bool AgtB = A && (!(B));

	for (int i = comp_size-2; i >= 0; i--)
	{
		X = X && ((A && B) || ((!(A)) && (!(B))));
		A = operand_A.get_bit(i);
		B = operand_B.get_bit(i);
	
		AgtB = AgtB || (A && !(B) && X);
	}

	//cout << AgtB;
	gt->write(AgtB);
}

#endif