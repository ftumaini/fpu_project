#ifndef FP_ADDER_HPP
#define FP_ADDER_HPP

/*
 * Floating point ALU internal module for performing sums/subtractions.
 * The sequence of operation is the following:
 * 1) denormalize the little operand (exponent adjust, mantissa shift)
 * 2) add the two mantissas together
 * 3) radix point adjustment for the result
 * 4) result normalization
 *
 * The total amount of clock periods needed to complete a single operation
 * is 11 (22 clock transitions)
 *
 * In order to perform subtractions, one extra input is provided ("opcode")
 */

#include "comp.hpp"
#include "lil_denorm.hpp"
#include "MuxGate.hpp"
#include "XorGate.hpp"

#define MANT_SIZE 23

class FloatingPointAdder : public sc_module
{
	public:
		FloatingPointAdder(sc_module_name fpa_name);
		
		sc_in<bool> clk;
		sc_in<sc_bv<1> > opcode;	// 0 = sum, 1 = sub
		sc_in<sc_bv<32> > in_A, in_B;
		sc_out<sc_bv<32> > out_S;
		
	private:
		sc_event write_event, update_event, select_event;
		
		/* thread(s) */
		void inner_loop();
		
		/* method(s) */
		void fpa_enable();
		void select_mantissas();
		void temp_update();
		void write_result();

		/* module class function(s) */
		unsigned leftZeroCount(sc_bv<MANT_SIZE+1> stream);
		
		unsigned clk_pnum;

		LilDenorm ld;
		CLA_Adder ma0, ma1, ma2;

		sc_signal<sc_bv<32> > d_A, d_B;
		sc_signal<bool> mant_A0[(MANT_SIZE+1)/3], mant_A1[(MANT_SIZE+1)/3], mant_A2[(MANT_SIZE+1)/3];
		sc_signal<bool> mant_B0[(MANT_SIZE+1)/3], mant_B1[(MANT_SIZE+1)/3], mant_B2[(MANT_SIZE+1)/3];
		sc_signal<bool> mant_S[MANT_SIZE+1];
		sc_signal<bool> carry_in_0, carry_in_1, carry_in_2;
		sc_signal<bool> carry_out_0, carry_out_1, carry_out_2, equal;
		
		sc_bv<32> s_read_A, s_read_B, temp;
		sc_bv<8> exp_buffer;
		bool enable, debugMode;

		/* upgrades for implement also subtraction operations */
		XorGate<1,3> Sign_Xor;
		MuxGate<MANT_SIZE+1,2,1> B_Mux;
		Comp<MANT_SIZE+1> Ma_Comp;

		sc_signal<bool> ld_zero, expAgtB, mA_eq_mB, mA_gt_mB;
		sc_signal<sc_bv<1> > sx0, sx1, sel_b;
		sc_signal<sc_bv<2*(MANT_SIZE+1)> > bb1_compl;
		sc_signal<sc_bv<MANT_SIZE+1> > b_mux_out, mant_A, mant_B;

		sc_bv<MANT_SIZE+1> d_B1;
		void binaryOneIncrement(sc_bv<24>& bw);
		void binaryOneDecrement(sc_bv<24>& bw);

		bool setResultSign();
};

#endif