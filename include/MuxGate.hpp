#ifndef MUX_GATE_HPP
#define MUX_GATE_HPP

#include <systemc.h>

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
class MuxGate : public sc_module {
	public:
		MuxGate(sc_module_name mux_name);

		sc_in<sc_bv<SEL_SIZE> > sel;
		sc_in<sc_bv<D_SIZE*D_NUM> > data_in;
		sc_out<sc_bv<D_SIZE> > data_out;

	private:
		void select();
};

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
MuxGate<D_SIZE, D_NUM, SEL_SIZE>::MuxGate(sc_module_name mux_name)
{
	SC_HAS_PROCESS(MuxGate);

	SC_METHOD(select);
	sensitive << sel << data_in;
}

template <unsigned D_SIZE, unsigned D_NUM, unsigned SEL_SIZE>
void MuxGate<D_SIZE, D_NUM, SEL_SIZE>::select()
{
	data_out.write(data_in.read().range(D_SIZE*sel.read().to_uint(),(D_SIZE)*(1+sel.read().to_uint())-1));
}

#endif