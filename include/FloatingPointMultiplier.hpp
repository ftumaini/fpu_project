#ifndef FP_MULTIPLIER_HPP
#define FP_MULTIPLIER_HPP

/*
 * Floating point ALU internal module for performing multiply operations.
 * The sequence of operation is the following:
 * 1) select product sign (operand signs xor-ed)
 * 2) add the two exponents together
 * 3) multiply the two mantissa (shifted right by 1 position - LSB will be lost)
 * 4) result normalization
 *
 * Underflow/overflow condition is evaluater using a 9-bit comparator after the
 * exponents sum (less than 127 = underflow, greater than 254 = overflow) 
 *
 * The total amount of clock periods needed to complete a single operation
 * is 67 (134 clock transitions)
 */

#include "comp.hpp"
#include "XorGate.hpp"
#include "MantissaMult.hpp"
#include "CLA_Adder.hpp"

#define MANT_SIZE 23

class FloatingPointMultiplier : public sc_module
{
	public:
		FloatingPointMultiplier(sc_module_name fpm_name);
		
		sc_in<bool> clk;
		sc_in<sc_bv<32> > in_A, in_B;
		sc_out<sc_bv<32> > out_P;
		
	private:
		sc_event outputReady;		

		/* method(s) */
		void operandsFetch();
		void outputAssembly();
		void truncateMantissa();
		void underOverFlowIn();
		void underOverFlowOut();
		
		/*
		* Since the two input exponent are biased (+127 offset),
		* their sum is "double biased" (+2*127 offset); so, we need
		* to restore the original bias by performing a subtraction,
		* that is equivalent to a 2-complement sum with -127 
		*/
		CLA_Adder exp_adder, increment_adder;
		
		MantMultiplier mm;
		XorGate<1,2> sign_select;

		Comp<9> comp_underflow, comp_overflow;

		sc_signal<sc_bv<1> > sA, sB, sProd;
		sc_signal<bool> exp_A[8], exp_B[8], exp_Prod_ob[9];
		sc_signal<bool> exp_cin[2];
		sc_signal<bool> amount[8], exp_Prod[8];
		sc_signal<bool> exp_cout;		
		sc_signal<sc_bv<F_SIZE> > mant_A, mant_B;
		sc_signal<sc_bv<2*F_SIZE> > mant_P_full;
		sc_signal<sc_bv<9> > c_under_0, c_under_1;
		sc_signal<sc_bv<9> > c_over_0, c_over_1;
		sc_signal<bool> c_under_eq, c_under_gt, c_over_eq, c_over_gt;
		
		sc_bv<8> exp_buffer;
		sc_bv<F_SIZE> mant_P;

		unsigned inner_bias;

		/* active high only if input exponents are both zero (useful for denormalized output) */
		bool denormOutput;

		/* NaN test flags */
		bool infByZero, zeroByInf, flagNaN;
};

#endif