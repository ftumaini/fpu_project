#ifndef ADDER_HPP
#define ADDER_HPP

#include "fulladder.hpp"

template <int add_size>
class Adder : public sc_module {
	public:
		Adder(sc_module_name add_name);
		
		sc_in<bool> opcode;
		sc_in<bool> in_A[add_size], in_B[add_size];
		sc_out<bool> res[add_size];
		sc_out<bool> overflow;

	private:
		sc_event choose_op, start_read, read_event, write_event;
	
		void read_opcode();
		void perform_addition();
		void elaborate_result();
		void read_inputs();
		void write_result();

		sc_vector<FullAdder> fav;
		sc_vector<sc_signal<bool> > fav_Cin;
		sc_vector<sc_signal<bool> > fav_Cout;
		
		sc_signal<bool> operand_A[add_size], operand_B[add_size];
		sc_signal<bool> result[add_size+1];
		sc_signal<bool> operation;
};

template <int add_size>
Adder<add_size>::Adder(sc_module_name add_name)
:fav("fa_",add_size), fav_Cin("Cin_",add_size), fav_Cout("Cout_",add_size)
{
	SC_HAS_PROCESS(Adder);

	SC_METHOD(perform_addition);
	for (int k=add_size-1; k>=0; k--) {
		sensitive << in_A[k] << in_B[k];
	}
	sensitive << opcode;
	dont_initialize();
	
	SC_METHOD(read_opcode);
	sensitive << choose_op;
	dont_initialize();

	SC_METHOD(read_inputs);
	sensitive << start_read;
	dont_initialize();

	SC_METHOD(elaborate_result);
	sensitive << read_event;
	dont_initialize();

	SC_METHOD(write_result);
	sensitive << write_event;
	dont_initialize();


	fav[0].A(operand_A[0]);
	fav[0].B(operand_B[0]);
	fav[0].Cin(fav_Cin[0]);
	for (int j=1; j<add_size; j++)
	{
		fav[j-1].Cout(fav_Cin[j]);
		fav[j-1].S(result[j-1]);
		fav[j].A(operand_A[j]);
		fav[j].B(operand_B[j]);
		fav[j].Cin(fav_Cout[j-1]);
		
	}
	fav[add_size-1].Cout(result[add_size]);
	fav[add_size-1].S(result[add_size-1]);
	
}

template <int add_size>
void Adder<add_size>::perform_addition()
{
	//cout << "{inputs are changed}" << endl;
		
	choose_op.notify(SC_ZERO_TIME);
}

template <int add_size>
void Adder<add_size>::read_opcode()
{
	operation = opcode.read();
	start_read.notify(SC_ZERO_TIME);
}

template <int add_size>
void Adder<add_size>::read_inputs()
{
	for (int i=0; i<add_size; i++) {
		operand_A[i] = in_A[i].read();
	}
	
	if (operation) {
		for (int i=0; i<add_size; i++) {
			operand_B[i] = !(in_B[i].read());
		}
	}
	else {
		for (int i=0; i<add_size; i++) {
			operand_B[i] = in_B[i].read();
		}
	}
	
	cout << "read inputs completed" << endl;// [ A : ";
	/*for (int i=0; i<add_size; i++) {
		cout << operand_A[i];
	}
	cout << ", B : ";
	for (int i=0; i<add_size; i++) {
		cout << operand_B[i];
	}
	cout << "]" << endl;	
*/
	read_event.notify(SC_ZERO_TIME);
}

template <int add_size>
void Adder<add_size>::elaborate_result()
{
	cout << "elaboration takes place";
	
	cout << " [ A : ";
	for (int i=0; i<add_size; i++) {
		cout << operand_A[i];
	}
	cout << ", B : ";
	for (int i=0; i<add_size; i++) {
		cout << operand_B[i];
	}
	cout << ", OP : " << operation << "]" << endl;	


	for (int j=0; j<add_size; j++) {
		res[j].write(result[j].read());
	}
	overflow.write(result[add_size].read());

	write_event.notify(SC_ZERO_TIME);
}

template <int add_size>
void Adder<add_size>::write_result()
{
	cout << "result has been written [S : ";	
	
	for (int i=0; i<add_size; i++) {
		cout << res[i];
	}
	cout << " ]" << endl;
}
#endif