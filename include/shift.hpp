#ifndef SHIFT_HPP
#define	SHIFT_HPP

#include <cmath>

template <int shift_size>
class Shift : public sc_module {
	public:
		Shift(sc_module_name shift_name, sc_bit direction);
		sc_in<sc_bv<shift_size> > in_shift;
		sc_in<sc_bv<8> > in_shamt;
		sc_out<sc_bv<shift_size> > out_shift;
		
	private:
		void perform_shift();

		sc_bit sh_dir; // shift direction [0:left, 1:right]
		sc_int sh_amt;
		sc_bv<shift_size> sh_data;
};

template <int shift_size>
Shift<shift_size>::Shift(sc_module_name shift_name)
: Shift(shift_name), sh_dir(direction), sh_amt(0)
{
	SC_THREAD(perform_shift)
	sensitive << in_shift << in_shamt;
}

template <int shift_size>
void Shift<shift_size>::perform_shift()
{
	sh_data = in_shift->read();
	
	// convert shift amount from bit vector 
	// representation to an integer value
	sc_bv<8> amount = in_shamt->read();
	for (int i=0; i<8; i++)
	{
		if (amount(1))
		{
			sh_amt = sh_amt + pow(2, i);
		}
	}

	if (sh_dir)
	{
		// perform right shift (logical)
		for (int j=0; j<(shift_size-sh_amt); j++)
		{
			sh_data(j) = sh_data(j+sh_amt);
		}	
	}
	else
	{
		// perform left shift (logical)
		for (int j=shift_size-1; j>=shamt; j--)
		{
			sh_data(j) = sh_data(j-sh_amt);
		}
	}

	out_shift->write(sh_data);
}

#endif