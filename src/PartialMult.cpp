#include <systemc.h>
#include "PartialMult.hpp"

PartialMultiplier::PartialMultiplier(sc_module_name pm_name)
{
	SC_HAS_PROCESS(PartialMultiplier);
	
	SC_METHOD(perform_multi);
	sensitive << Fa << Fb;
	dont_initialize();
}

void PartialMultiplier::perform_multi()
{
	sc_bv<23> fa, fb;
	
	fa = Fa->read();
	fb = Fb->read();

	for (unsigned i = 0; i < F_SIZE*F_SIZE; i++) {
		p_buffer[i] = (fa[i % F_SIZE] & fb[i / F_SIZE]);
	}
	
	Pr->write(p_buffer);
}