#include <systemc.h>
#include "lil_denorm.hpp"

LilDenorm::LilDenorm(sc_module_name ld_name)
:exp_adder("8-bit_exponent_adder"), exp_comp("8-bit_exponents_comparator"), 
mant_size(23), clk_pnum(0), shamt(0), zero_flag(false), debugMode(false)
{
	SC_HAS_PROCESS(LilDenorm);

	exp_comp.in_A(in_comp_A);
	exp_comp.in_B(in_comp_B);
	exp_comp.eq(AeqB);
	exp_comp.gt(AgtB);

	exp_adder.clk(this->clk);
	exp_adder.in_carry(op);
	for (int i=0; i<8; i++)
	{
		exp_adder.in_A(operand_A[i]);
		exp_adder.in_B(operand_B[i]);
		exp_adder.out_S(mant_shift[i]);
	}
	exp_adder.out_carry(ovf);

	SC_METHOD(ld_enable);
	sensitive << in_A << in_B;
	dont_initialize();

	SC_THREAD(internal_loop);
	sensitive << clk;
	dont_initialize();	
	
	SC_METHOD(find_minor);
	sensitive << read_event;
	dont_initialize();	
	SC_METHOD(exp_offset);
	sensitive << add_event;
	dont_initialize();	
	SC_METHOD(denormalization);
	sensitive << denorm_event;
	dont_initialize();	
	SC_METHOD(propagate_results);
	sensitive << write_event;
	dont_initialize();
}

void LilDenorm::ld_enable()
{
	if (!(enable)) {
		clk_pnum = 0;
		enable = true;
		if (debugMode) {
			cout << "LD: turn on the adder" << endl;
		}
	}
}

void LilDenorm::internal_loop()
{
	while(true) {
		wait();
		if (enable) {
		//cout << "#" << clk_pnum << "#" << endl;
		switch (clk_pnum) {
			case 0 : {
				read_event.notify();
				break;
			}
			case 1 : {
				add_event.notify();
				break;
			}
			case 5 : {
				denorm_event.notify();
				break;
			}
			case 6 : {
				write_event.notify();
				break;
			}
			case 7 : {
				enable = false;
				break;
			}
			default : break;	
		}
		clk_pnum++;
		}
	}
}

void LilDenorm::find_minor()
{
	if (debugMode) {
		cout << "LD: Inputs fetch step - ";
		cout << in_A.read() << " , " << in_B.read() << endl;
	}
	input_A = in_A.read();
	input_B = in_B.read();
}
void LilDenorm::exp_offset()
{
	sc_bv<8> operand_B_1_compl;

	//cout << "#input_A = " << input_A << endl;
	//cout << "#input_B = " << input_B << endl;

	in_comp_A = input_A.range(30,mant_size);
	in_comp_B = input_B.range(30,mant_size);

	if (debugMode) {
		cout << "LD: Exponents comparation and offset calculus ";
		cout << "[" << std::dec << input_A.range(30,mant_size);
		cout << "-" << input_B.range(30,mant_size) << "]" << endl;
	}

	for (int i=0; i<8; i++) {
		operand_B_1_compl[i] = !(input_B[i+mant_size]);
	}
	binaryOneIncrement(operand_B_1_compl);

	for (int i=0; i<8; i++) {
		operand_A[7-i] = input_A[i+mant_size];
		operand_B[7-i] = operand_B_1_compl[i];
	}
}
void LilDenorm::denormalization()
{
	sc_bv<8> mant_shift_buffer;

	for (int i=0; i<8; i++) {
		mant_shift_buffer[i] = mant_shift[i];
	}
	shamt = boolVectorToInt(mant_shift_buffer);

	if (debugMode) {
		cout << "LD: Denormalization step (";
		for (int i=0; i<8; i++) {
			cout << mant_shift[i];
		}
		cout << ") ";
		cout << "{shift amount is equal to " << shamt << "} - " << endl;
	}

	if (shamt > 0) 
	{
		// operand 'B' has to be denormalize
		if (debugMode) {
			cout << "B has to be denormalize" << endl;
		}
		output_A = input_A;
		output_B[31] = input_B[31];
		for (int i=1; i<=8; i++) {
			output_B[31-i] = input_A[31-i];
		}
					
		if (shamt < mant_size) {
			for (int i = 0; i<=shamt; i++) {
			output_B[mant_size-1-i] = false;
			}
			output_B[mant_size-shamt] = true;
			output_B.range(mant_size-shamt-1,0) = input_B.range(mant_size-1,shamt);
			zero_flag = false;
		}
		else {
			// operand 'B' exponent is so small that mantissa "shift out" completely
			for(int j=mant_size-1; j>=0; j--) {
				output_B[j] = false;
			}
			zero_flag = true;
		}
	}
	else if (shamt < 0)
	{
		// operand 'A' has to be denormalize
		if (debugMode) {
			cout << "A has to be denormalize" << endl;
		}

		shamt = -1 * shamt;
		output_B = input_B;
		output_A[31] = input_A[31];
		for (int i=1; i<=8; i++) {
			output_A[31-i] = input_B[31-i];
		}
				
		if (shamt < mant_size) {
			for (int i = 0; i<shamt; i++) {
				output_A[mant_size-1-i] = false;
			}
			output_A[mant_size-shamt] = true;	
			output_A.range(mant_size-shamt-1,0) = input_A.range(mant_size-1,shamt);
			zero_flag = false;
		}
		else {
			for(int j=mant_size-1; j>=0; j--) {
				output_A[j] = false;
			}
			zero_flag = true;
		}
	}
	else
	{
		// exp(A) == exp(B) [no need for denormalizing exponents]
		output_A = input_A;
		output_B = input_B;
	}
}

void LilDenorm::propagate_results()
{
	out_A.write(output_A);
	out_B.write(output_B);
	exp_eq.write(AeqB);
	exp_gt.write(AgtB);
	man_ovf.write(zero_flag);	

	if (debugMode) {
		cout << "LD: write outputs";
		if (AeqB) {
			cout << " with no denormalization";
		}
		else {
			cout << " with major exponent (at input #" << AgtB << ")";
		}
		cout << endl;
		cout << "   [ A :" << output_A << ", B : " << output_B << "]" << endl;
		cout << "   [ Zero flag is set to " << zero_flag << "]" << endl;
	}
}

/*
 * Bit vector to unsigned value conversion
 */
int LilDenorm::boolVectorToInt(sc_bv<8> bv)
{
	int sign_decision = 1;
	int count = 0;
	unsigned weight = 128;

	if (!(AeqB || AgtB)) { // result will be negative (non-positive)
		for (unsigned i=0; i<8; i++) {
			bv[i] = !(bv[i]);
		}
		count++;
		sign_decision = -1;
	}
	
	for (unsigned k=0; k<8; k++) {
		count = (bv[k]) ? (count + weight) : count;
		weight = weight / 2;
	}
	
	return (sign_decision*count);
}

void LilDenorm::binaryOneIncrement(sc_bv<8>& bw)
{
	unsigned index = 0;
	while (index < 8) {
		if (bw[index]) {
			bw[index] = false;
		}
		else {
			break;
		}
		index++;	
	}

	if (index < 8) {
		bw[index] = true;
	}
}
void LilDenorm::binaryOneDecrement(sc_bv<8>& bw)
{
	unsigned index = 0;
	while (index < 8) {
		if (!(bw[index])) {
			bw[index] = true;
		}
		else {
			break;
		}
		index++;	
	}

	if (index < 8) {
		bw[index] = false;
	}
}