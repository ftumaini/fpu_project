#include <systemc.h>
#include "FloatingPointAdder.hpp"

FloatingPointAdder::FloatingPointAdder(sc_module_name fpa_name)
:ld("denormalizer"), Sign_Xor("sign_xor_gate"), Ma_Comp("mantissa_comparator"), 
B_Mux("B_mantissa_mux"), ma0("mantissa_adder_0_to_7"),
ma1("mantissa_adder_8_to_15"), ma2("mantissa_adder_16_to_23"),
enable(false), debugMode(false)
{
	SC_HAS_PROCESS(FloatingPointAdder);

	/* inner ports binding */

	Ma_Comp.in_A(mant_A);
	Ma_Comp.in_B(mant_B);
	Ma_Comp.eq(mA_eq_mB);
	Ma_Comp.gt(mA_gt_mB);

	Sign_Xor.data_in(sx0);
	Sign_Xor.data_in(sx1);
	Sign_Xor.data_in(this->opcode);
	Sign_Xor.data_out(sel_b);

	ld.clk(this->clk);
	ld.in_A(this->in_A); 
	ld.in_B(this->in_B); 
	ld.out_A(this->d_A);
	ld.out_B(this->d_B);
	ld.exp_eq(this->equal);
	ld.exp_gt(this->expAgtB);
	ld.man_ovf(this->ld_zero);	

	B_Mux.sel(sel_b);
	B_Mux.data_in(bb1_compl);
	B_Mux.data_out(b_mux_out);
	
	ma0.clk(this->clk);
	ma1.clk(this->clk);
	ma2.clk(this->clk);
	ma0.in_carry(carry_in_0);
	ma1.in_carry(carry_in_1);
	ma2.in_carry(carry_in_2);
	for (int i=0; i<(MANT_SIZE+1)/3; i++) {
		ma0.in_A(this->mant_A0[(MANT_SIZE+1)/3-1-i]); 
		ma0.in_B(this->mant_B0[(MANT_SIZE+1)/3-1-i]);
		ma0.out_S(this->mant_S[(MANT_SIZE+1)/3-1-i]);
		ma1.in_A(this->mant_A1[(MANT_SIZE+1)/3-1-i]); 
		ma1.in_B(this->mant_B1[(MANT_SIZE+1)/3-1-i]);
		ma1.out_S(this->mant_S[2*(MANT_SIZE+1)/3-1-i]);
		ma2.in_A(this->mant_A2[(MANT_SIZE+1)/3-1-i]); 
		ma2.in_B(this->mant_B2[(MANT_SIZE+1)/3-1-i]);
		ma2.out_S(this->mant_S[MANT_SIZE-i]);
	}
	ma0.out_carry(carry_out_0);
	ma1.out_carry(carry_out_1);
	ma2.out_carry(carry_out_2);

	SC_THREAD(inner_loop);
	sensitive << clk;

	SC_METHOD(fpa_enable);
	sensitive << d_A << d_B;
	dont_initialize();
	
	SC_METHOD(select_mantissas)
	sensitive << select_event;
	dont_initialize();

	SC_METHOD(temp_update)
	sensitive << update_event;
	dont_initialize();

	SC_METHOD(write_result)
	sensitive << write_event;
	dont_initialize();
}

void FloatingPointAdder::inner_loop()
{
	while(true) {
		wait();
		if (enable) {
		switch (clk_pnum) {
			case 0 : {
				sx0.write(in_A.read().range(31,31));
				sx1.write(in_B.read().range(31,31));
				break;
			}
			case 1 : {
				select_event.notify();
				break;
			}
			case 5 : {
				if (debugMode) {
					cout << "activate mantissa adder 1/3 (";
					cout << s_read_A.range((MANT_SIZE+1)/3-1,0) << " - ";
					cout << b_mux_out.read().range(2*(MANT_SIZE+1)/3,MANT_SIZE) << ")" << endl;
				}
				for (int i=0; i<((MANT_SIZE+1)/3); i++) {
					mant_A0[i].write(s_read_A[i]);
					mant_B0[i].write(b_mux_out.read()[MANT_SIZE-i]);			
				}
				break;
			}
			case 9 : {
				if (debugMode) {
					cout << "activate mantissa adder 2/3 (";
					cout << s_read_A.range((2*(MANT_SIZE+1)/3)-1,(MANT_SIZE+1)/3) << " - ";
					cout << b_mux_out.read().range((MANT_SIZE+1)/3,(2*(MANT_SIZE+1)/3)-1) << ") ";
					cout << " <- " << carry_out_0.read() << " carry in" << endl;
				}
				carry_in_1.write(carry_out_0.read());
				for (int i=0; i<((MANT_SIZE+1)/3); i++) {
					mant_A1[i].write(s_read_A[((MANT_SIZE+1)/3)+i]);
					mant_B1[i].write(b_mux_out.read()[2*((MANT_SIZE+1)/3)-1-i]);			
				}
				break;
			}
			case 13 : {
				if (debugMode) {
					cout << "activate mantissa adder 3/3 (";
					cout << s_read_A.range((3*(MANT_SIZE+1)/3)-1,2*(MANT_SIZE+1)/3) << " - ";
					cout << b_mux_out.read().range(0,((MANT_SIZE+1)/3)-1) << ") ";
					cout << " <- " << carry_out_1.read() << " carry in" << endl;
				}
				carry_in_2.write(carry_out_1.read());
				for (int i=0; i<((MANT_SIZE+1)/3-1); i++) {
					mant_A2[i].write(s_read_A[(2*(MANT_SIZE+1)/3)+i]);
					mant_B2[i].write(b_mux_out.read()[((MANT_SIZE+1)/3)-1-i]);			
				}
				mant_A2[7].write(mant_A.read()[MANT_SIZE]);
				mant_B2[7].write(b_mux_out.read()[0]);
				break;
			}
			case 17 : {
				update_event.notify();
				break;
			}
			case 18 : {
				write_event.notify();
				break;
			}
			case 19 : {
				enable = false;
				break;
			}
			default : break;	
		}
		clk_pnum++;
		}	
	}
}

void FloatingPointAdder::fpa_enable()
{
	if (!(enable)) {
		clk_pnum = 0;
		enable = true;
		if (debugMode) {
			cout << "turn on the FP_Unit" << endl;
		}
	}
}

void FloatingPointAdder::select_mantissas()
{
	if ((sel_b.read() == "1") && (sx0.read() == "1")) {	// since in a subtraction operand ordering matters,
						// when adding -A to +B or subtracting -B to -A,
						// operand must be swapped (because only B is "complementable")
		s_read_B = d_A;
		s_read_A = d_B;
		if (debugMode) {
			cout << endl << "*operand swapping because a sub is requested and first operand is negative" << endl;
		}
	}
	else {
		s_read_A = d_A;
		s_read_B = d_B;
	}

	if (debugMode) {
		cout << "operands A (" << s_read_A << ") ";
		cout << "and B (" << s_read_B << ") fetch after update" << endl;
		cout << "operands signs are:" << endl;
		cout << "   A = " << sx0.read() << endl;
		cout << "   B = " << sx1.read() << endl;
		cout << "   [operation requested is " << this->opcode.read() << "]";
		cout << "      and the mux selector is at position " << sel_b.read() << endl;
	}
	
	for (int i=0; i<MANT_SIZE; i++) {
		d_B1[i] = !(s_read_B[i]);
	}
	if ((equal.read() || (!(expAgtB.read()))) &&
		(s_read_B.range(30,MANT_SIZE).or_reduce())) {
		d_B1[MANT_SIZE] = false;
		this->binaryOneIncrement(d_B1);
		bb1_compl.write((d_B1,("1",s_read_B.range(MANT_SIZE-1,0))));
		mant_B.write(("1",s_read_B.range(MANT_SIZE-1,0)));
	}
	else {
		d_B1[MANT_SIZE] = true;
		this->binaryOneIncrement(d_B1);
		bb1_compl.write((d_B1,("0",s_read_B.range(MANT_SIZE-1,0))));
		mant_B.write(("0",s_read_B.range(MANT_SIZE-1,0)));
	}

	if ((expAgtB.read() || equal.read()) &&
		(s_read_A.range(30,MANT_SIZE).or_reduce())) {
		mant_A.write(("1",s_read_A.range(MANT_SIZE-1,0)));
	}
	else {
		mant_A.write(("0",s_read_A.range(MANT_SIZE-1,0)));
	}			
}

void FloatingPointAdder::temp_update()
{
	unsigned exp_offset = 0;
	sc_bv<MANT_SIZE+1> mantissa_buffer;
	
	for (int j=0; j<=MANT_SIZE; j++) {
		mantissa_buffer[j] = mant_S[j];	
	}
	
	if ((sel_b.read() == "1") && ((sx0.read() == "1") || (!(mA_gt_mB)))) {	// de-complement result mantissa
		cout << "after a sub, mantissa is de complemented" << endl;
		for (int j=0; j<=MANT_SIZE; j++) {
			mantissa_buffer[j] = !(mantissa_buffer[j]);	
		}
		this->binaryOneIncrement(mantissa_buffer);
	}

	exp_buffer = s_read_A.range(30,MANT_SIZE);
	
	// choose result sign, according to operands signs (and operation code)
	temp[31] = setResultSign();
	
	if		// one of the 2 operands is "Not-a-Number" (NaN)
	((exp_buffer.and_reduce() && mantissa_buffer.or_reduce())
		|| 	// or the result is zero
	(!(exp_buffer.or_reduce() || mantissa_buffer.or_reduce())) 
		|| 	// or the carry-out lead to an exponent overflow
	((exp_buffer.range(7,1).and_reduce()) && carry_out_2 && (sel_b.read() == "0"))) {
			temp.range(30,0) = (exp_buffer,mantissa_buffer.range(MANT_SIZE-1,0));
	}
	else if (carry_out_2 
		&& (sel_b.read() == "0")) { 	// overflow in the mantissa sum (the hidden '1' is the overflow bit);
						// so, the final mantissa must be right-shifted by 1 bit
						// [LSB will be lost]
		cout << "   --overflow in a sum--" << endl;
		ld.binaryOneIncrement(exp_buffer);
		temp.range(30,MANT_SIZE) = exp_buffer;
		temp.range(MANT_SIZE-1,0) = mantissa_buffer.range(MANT_SIZE-1,0) >> 1;
		temp[MANT_SIZE-1] = mant_S[MANT_SIZE];
	}
	else {
		temp.range(30,MANT_SIZE) = exp_buffer;
						
		if  (!(exp_buffer.or_reduce())) {	// input are in denormalized notation (exp = 0)
			exp_offset = (mant_S[MANT_SIZE]) ? 1 : 0;
			temp.range(MANT_SIZE-1,0) = mantissa_buffer.range(MANT_SIZE-1,0);
		}
		else if (mantissa_buffer.or_reduce()) {	// for a non-zero mantissa buffer,
							// count number of zero left to the MSB (the hidden '1')
							// and shift it left to the same amount for alignment
			exp_offset = leftZeroCount(mantissa_buffer);
			temp.range(MANT_SIZE-1,0) = mantissa_buffer.range(MANT_SIZE-1,0) << exp_offset;
		}
		else {
			exp_offset = 1;
			temp.range(MANT_SIZE-1,0) = mantissa_buffer.range(MANT_SIZE-1,0) << exp_offset;
		}
		
		while (exp_offset > 0) {
			if (sel_b.read() == "0") {
				ld.binaryOneIncrement(exp_buffer);
			}
			else {
				ld.binaryOneDecrement(exp_buffer);
			}
			exp_offset--;
			if (exp_buffer.and_reduce()) {
				temp.range(30,MANT_SIZE) = exp_buffer;
			}
		}
		if (!(temp.range(30,MANT_SIZE).and_reduce())) {
			temp.range(30,MANT_SIZE) = exp_buffer;
		}
		if ((!(mantissa_buffer.or_reduce())) && equal.read()) {
			temp.range(30,MANT_SIZE) = "00000000";
		}
	}
	

	if (debugMode) {
		cout << "mantissas sum has been performed as follows:" << endl;
		cout << "   " << mant_A.read() << " + " << endl;
		cout << "   " << b_mux_out.read().range(0,MANT_SIZE) << " = " <<  endl << "   ";
		for (int j=MANT_SIZE; j>=0; j--) {
			cout << mant_S[j];
		}
		cout << endl;
		if (carry_out_2) {
			cout << "   (overflow)";
		}
		cout << endl;
	}	
}

void FloatingPointAdder::write_result()
{
	out_S.write(temp);
	
	if (debugMode) {
		cout << "output writeback (after update)" << endl;
		cout << "   result is " << temp << endl;
	}
}

bool FloatingPointAdder::setResultSign()
{
	if (expAgtB.read()) {
		return this->d_A.read()[31];
	}
	else if (!(equal.read())) {
		return (this->opcode.read() == "0") ? this->d_B.read()[31] : !(this->d_B.read()[31]);
	}
	else {
		if ((sel_b.read() == "0") || (mA_gt_mB)) { 
			return this->s_read_A[31];
		}
		else {
			return this->s_read_B[31];
		}
	}
}

/*
 * Increment/decrement by 1 unit a 24-bit word (little endian)
 */
void FloatingPointAdder::binaryOneIncrement(sc_bv<24>& bw)
{
	unsigned index = 0;
	while (index < 24) {
		if (bw[index]) {
			bw[index] = false;
		}
		else {
			break;
		}
		index++;	
	}

	if (index < 24) {
		bw[index] = true;
	}
}
void FloatingPointAdder::binaryOneDecrement(sc_bv<24>& bw)
{
	unsigned index = 0;
	while (index < 24) {
		if (!(bw[index])) {
			bw[index] = true;
		}
		else {
			break;
		}
		index++;	
	}

	if (index < 24) {
		bw[index] = false;
	}
}


/*
 * Count number of trailing zero left of a number (aligned to MSB)
 */
unsigned FloatingPointAdder::leftZeroCount(sc_bv<MANT_SIZE+1> stream)
{
	unsigned c = 0;
	while(c <= MANT_SIZE) {
		
		if (stream[MANT_SIZE-c]) {
			return c;
		}
		
		c++;
	}
	return c;
}