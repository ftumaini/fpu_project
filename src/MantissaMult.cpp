#include <iomanip>
#include <systemc.h>
#include "MantissaMult.hpp"

MantMultiplier::MantMultiplier(sc_module_name mant_m_name)
:pm_Adder("pm_Adder",3), pm_Core("Core_Partial_Multiplier"), 
bufferPos(0), clk_pnum(0), debugMode(false)
{
	SC_HAS_PROCESS(MantMultiplier);

	pm_Core.Fa(this->Ma);
	pm_Core.Fb(this->Mb);
	pm_Core.Pr(pm_buffer);
	
	for (unsigned i=0; i<3; i++) {
		pm_Adder[i].clk(this->Clk);
		pm_Adder[i].in_carry(pm_cin[i]);
		for (int j=7; j>=0; j--) {
			pm_Adder[i].in_A(pm_fact_A[8*i+j]);
			pm_Adder[i].in_B(pm_fact_B[8*i+j]);
			pm_Adder[i].out_S(pm_prod[8*i+j]);
		}
		pm_Adder[i].out_carry(pm_cout[i]);
	}

	SC_THREAD(inner_loop);
	sensitive << Clk;
	dont_initialize();

	SC_METHOD(writeProduct);
	sensitive << productComplete;
	dont_initialize();

	SC_METHOD(inputsMul);
	sensitive << pm_buffer;
	dont_initialize();
}

// Module main thread

void MantMultiplier::inner_loop()
{
	while(true) {
		wait();
		if (enable) {
		switch (clk_pnum % 12) {
			case 0 : {
				if ((clk_pnum/12) >= 22) {
					for (unsigned i=0; i<F_SIZE; i++) {
						buffer_pout[F_SIZE-1+i] = pm_prod[i+1];
					}
					buffer_pout[2*F_SIZE-1] = pm_cout[2];
					productComplete.notify();
					enable = false;
				}
				else {
				if (debugMode) {
				cout << "ROUND " << ((clk_pnum/12)+1) << "/" << "22:" << endl;
				//cout << " sum up another partial products (stage 0)..." << endl;	
				}
				partialProductsDes(0);
				}
				break;
			}	
			case 4 : {
				//cout << " sum up another partial products (stage 1)..." << endl;	
				partialProductsDes(1);
				break;
			}
			case 8 : {
				//cout << " sum up another partial products (stage 2)..." << endl;	
				partialProductsDes(2);
				bufferPos += F_SIZE;
				break;
			}
			case 11 : {
				if ((debugMode) && ((clk_pnum/12) > 0)) {
				cout << setw(2*F_SIZE) << buffer_pout.range((clk_pnum/12-1)%(2*F_SIZE-1),0) << endl;
				cout << "  [still " << (21-(clk_pnum/12)) << " sum to go...]" << endl;
				}
				
				break;
			}
			default : {
				break;
			}
		}
		clk_pnum++;
		}
	}
}

// Module System-C Method(s)

void MantMultiplier::inputsMul()
{
	if (!(enable)) {
		if (debugMode) {
			cout << "finish " << F_SIZE << "-by-1 multiplications" << endl;
		}
		clk_pnum = 0;
		enable = true;
		bufferPos = 0;
	}
}
void MantMultiplier::writeProduct()
{
	if (debugMode) {
		cout << endl << "! OUTPUT READY (" << buffer_pout << ") !" << endl;
	}

	Prod.write(buffer_pout);
}

// Module class function(s)

void MantMultiplier::carryUpdate(unsigned index)
{
	if (debugMode) {
	cout << " updating carry in #" << index+1;
	cout << " with carry out #" << index;
	cout << " [new value is " << pm_cout[index] << "]" << endl;
	}
	pm_cin[(index+1)%3] = pm_cout[index];
}
void MantMultiplier::partialProductsDes(unsigned stageId)
{
	unsigned adder_dwidth = 8;
	unsigned start_offset = 0;
	unsigned pm_limit_A = 8;
	
	switch (stageId) {
		case 0 : {
			// each partial product is shifted left by 1-bit
			// (like in a column-multiplication)
			start_offset = 1;
			adder_dwidth = 7;
			pm_fact_B[0] = false;
			break;
		}
		case 2 : {
			pm_limit_A = 7;
			break;
		}
		default : {
			break;
		}
	}
	

	if (bufferPos == 0) {
		
		if (stageId == 2) {
			buffer_pout[0] = pm_buffer.read()[0];
			bufferPos += F_SIZE;
		}
			
		// intialize the first 3-stage add operation
		for (unsigned i=0; i<pm_limit_A; i++) {
			pm_fact_A[stageId*8+i] = pm_buffer.read()[stageId*8+i];
		}
		for (unsigned i=0; i<adder_dwidth; i++) {
			pm_fact_B[stageId*8+start_offset+i] = pm_buffer.read()[stageId*8+(start_offset-1)+F_SIZE+i];
		}
		
		
	}
	else {
		if (stageId != 0) {
			carryUpdate(stageId-1);
		}
		if (stageId == 1) {
			buffer_pout[bufferPos/F_SIZE-1] = pm_prod[0];
		}
		if (stageId == 2) {
			// the 3rd-adder has only 7 bit of valid input data
			// (the MSB is related to the previous stage final carry out)
			pm_fact_A[F_SIZE] = pm_cout[2];
			pm_limit_A = 7;
		}
						
		// update the following adder inputs (along the chain)
		for (unsigned i=0; i<pm_limit_A; i++) {
			//cout << pm_prod[pm_limit_A-i+stageId*8];
			pm_fact_A[stageId*8+i] = pm_prod[stageId*8+1+i];
		}
		for (unsigned i=0; i<adder_dwidth; i++) {
			pm_fact_B[stageId*8+start_offset+i] = pm_buffer.read()[bufferPos+(start_offset-1)+stageId*8+i];
		}
		
		if ((debugMode) && (stageId == 0)) {
	
			cout << endl;
			cout << setw(F_SIZE+1-(bufferPos/F_SIZE-1));
			for (unsigned k=0; k<=F_SIZE; k++) {
				cout << pm_fact_A[F_SIZE-k];
			}
			cout <<  " +" << endl;
			cout << setw(F_SIZE+1-(bufferPos/F_SIZE-1));
			for (unsigned k=0; k<=F_SIZE; k++) {
				cout << pm_fact_B[F_SIZE-k];
			}
			cout << " = " << endl;
			cout << "--------------------------------" << endl;
			cout << setw(F_SIZE+1-(bufferPos/F_SIZE-1));
			for (unsigned k=0; k<=F_SIZE; k++) {
				cout << pm_prod[F_SIZE-k];
			}
			cout << endl << endl;
		}
			
	}
}