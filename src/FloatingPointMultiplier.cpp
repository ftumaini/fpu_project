#include <systemc.h>
#include "FloatingPointMultiplier.hpp"

FloatingPointMultiplier::FloatingPointMultiplier(sc_module_name fpm_name)
:sign_select("sign_xor_gate_selector"), exp_adder("exponent_adder"),
mm("mantissa_multiplier"), increment_adder("increment_by_1_adder"),
comp_underflow("underflow_test_comparator"), comp_overflow("overflow_test_comparator")
{
	SC_HAS_PROCESS(FloatingPointMultiplier);

	amount[0] = true;
	for (unsigned i=1; i<7; i++) {
		amount[i] = false;
	}
	amount[7] = true;

	/* inner ports binding */

	sign_select.data_in(sA);
	sign_select.data_in(sB);
	sign_select.data_out(sProd);

	comp_underflow.in_A(c_under_0);
	comp_underflow.in_B(c_under_1);
	comp_underflow.eq(c_under_eq);
	comp_underflow.gt(c_under_gt);
	
	comp_overflow.in_A(c_over_0);
	comp_overflow.in_B(c_over_1);
	comp_overflow.eq(c_over_eq);
	comp_overflow.gt(c_over_gt);
	
	exp_adder.clk(this->clk);
	exp_adder.in_carry(exp_cin[0]);
	for (int i=0; i<(MANT_SIZE+1)/3; i++) {
		exp_adder.in_A(this->exp_A[(MANT_SIZE+1)/3-1-i]); 
		exp_adder.in_B(this->exp_B[(MANT_SIZE+1)/3-1-i]);
		exp_adder.out_S(this->exp_Prod_ob[(MANT_SIZE+1)/3-1-i]);
	}
	exp_adder.out_carry(exp_Prod_ob[8]);

	increment_adder.clk(this->clk);
	increment_adder.in_carry(exp_cin[1]);
	for (int i=0; i<(MANT_SIZE+1)/3; i++) {
		increment_adder.in_A(this->exp_Prod_ob[(MANT_SIZE+1)/3-1-i]); 
		increment_adder.in_B(this->amount[(MANT_SIZE+1)/3-1-i]);
		increment_adder.out_S(this->exp_Prod[i]);
	}
	increment_adder.out_carry(exp_cout);

	mm.Clk(this->clk);
	mm.Ma(mant_A);
	mm.Mb(mant_B);
	mm.Prod(mant_P_full);

	SC_METHOD(operandsFetch)
	sensitive << in_A;
	sensitive << in_B;
	dont_initialize();

	SC_METHOD(underOverFlowIn)
	for (int i=0; i<(MANT_SIZE+1)/3; i++) {
		sensitive << exp_Prod_ob[i];
	}
	dont_initialize();	

	SC_METHOD(underOverFlowOut)
	sensitive << c_under_eq;
	sensitive << c_under_gt;
	sensitive << c_over_eq;
	sensitive << c_over_gt;
	dont_initialize();	

	SC_METHOD(truncateMantissa)
	sensitive << mant_P_full;
	dont_initialize();

	SC_METHOD(outputAssembly)
	sensitive << outputReady;
	dont_initialize();
}

/*
* Split each operand in its 3 fields (sign,exponent,mantissa)
*/
void FloatingPointMultiplier::operandsFetch()
{
	denormOutput = true;
	infByZero = true;
	zeroByInf = true;
	flagNaN = false;
	inner_bias = 0;

	sA.write(in_A.read().range(31,31));
	sB.write(in_B.read().range(31,31));
	
	for (unsigned i=0; i<8; i++) {
		exp_A[i] = in_A.read()[F_SIZE+i];
		exp_B[i] = in_B.read()[F_SIZE+i];

		if (!(in_A.read()[F_SIZE+i] && (!(in_B.read()[F_SIZE+i])))) {
			infByZero = false;
		}

		if (!(in_B.read()[F_SIZE+i] && (!(in_A.read()[F_SIZE+i])))) {
			zeroByInf = false;
		}
	}
	
	// NaN product conditions check
	if (!(in_A.read().range(F_SIZE-1,0).or_reduce() || in_B.read().range(F_SIZE-1,0).or_reduce())) {  
		if (infByZero || zeroByInf) {
			flagNaN = true;
		}		
	}
	
	// Check if operands are in normalized form (or not)
	// [Note that NaN presence on inputs is already detected and managed in a "parent" module]
	if (in_A.read().range(F_SIZE+7,F_SIZE).or_reduce()) {
		mant_A = ("1",in_A.read().range(F_SIZE-1,1));
		inner_bias++;
		denormOutput = false;
	}
	else {
		mant_A = in_A.read().range(F_SIZE-1,0);
	}
	
	if (in_B.read().range(F_SIZE+7,F_SIZE).or_reduce()) {
		mant_B = ("1",in_B.read().range(F_SIZE-1,1));
		inner_bias++;
		denormOutput = false;
	}
	else {
		mant_B = in_B.read().range(F_SIZE-1,0);
	}	
}

/*
* Update underflow/overflow comparators with current sum exponent
*/
void FloatingPointMultiplier::underOverFlowIn()
{
	sc_bv<9> temp_exp;

	for (unsigned i=0; i<=8; i++) {
		temp_exp[i] = exp_Prod_ob[i];
	}

	c_under_0.write(temp_exp);
	c_under_1.write(127+inner_bias);
	c_over_0.write(temp_exp);
	c_over_1.write(3*127+inner_bias);
}

/*
* Detect underflow or overflow condition (flags active high)
*/
void FloatingPointMultiplier::underOverFlowOut()
{
	//cout << c_under_0 << endl;
	//cout << c_under_1 << " - " << c_over_1 << endl;
	if (!(c_under_eq || c_under_gt)) {
		//cout << "underflow on the output exponent (less than -126)" << endl;
		for (unsigned i=0; i<8; i++) {
			exp_buffer[7-i] = false;
		}
	}
	else if (c_over_gt) {
		//cout << "overflow on the output exponent (greater than +127)" << endl;
		for (unsigned i=0; i<8; i++) {
			exp_buffer[7-i] = true;
		}
	}	
}

void FloatingPointMultiplier::truncateMantissa()
{
	unsigned i = 0;
	unsigned start_index = F_SIZE;
	
	// find the MSB for the mantissa (the "hidden 1.")
	// or, if there is not, take the lower half of the mantissa
	while ((i<F_SIZE) && (start_index == F_SIZE)) {
		if (mant_P_full.read()[2*F_SIZE-1-i]) {
			start_index = 2*F_SIZE-1-i;
		}
		i++;
	}

	if (denormOutput) {
		start_index = 2*F_SIZE;
	}

	if (c_over_gt || (!(c_under_eq || c_under_gt))) {
		mant_P = 0;
	}
	else {
		mant_P = mant_P_full.read().range(start_index-1,start_index-F_SIZE);
	}

	outputReady.notify(SC_ZERO_TIME);
}

void FloatingPointMultiplier::outputAssembly()
{
	// output must be NaN
	if (flagNaN) {
		out_P.write((sProd.read(),"1111111111111111111111111111111"));
	}
	else {
		if (denormOutput) {
			exp_buffer = 0;	
		} else {  
		if (!((c_over_gt || (c_over_eq && mant_P_full.read()[2*F_SIZE-1])) 
			|| (!(c_under_eq || c_under_gt)))) {
			for (unsigned i=0; i<8; i++) {
				exp_buffer[7-i] = exp_Prod[i];
			}
				
			if (mant_P_full.read()[2*F_SIZE-1]) {
			//cout << "final exponent need to be incremented" << endl;
			// (because product modulus is greater or equal than 2)
				for (unsigned i=0; i<8; i++) {
					if (exp_buffer[i]) {
						exp_buffer[i] = false;
					} else {
						exp_buffer[i] = true;
						break;
					}
				}
			}
		}
		}
		out_P.write((sProd.read(),exp_buffer,mant_P));
	}
}