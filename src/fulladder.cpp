#include <systemc.h>
#include "fulladder.hpp"

FullAdder::FullAdder(sc_module_name fa_name)
:debugMode(false)
{
	SC_HAS_PROCESS(FullAdder);

	SC_METHOD(inputs_read);
	sensitive << A << B << Cin;
	dont_initialize();

	SC_METHOD(full_addition);
	sensitive << fa_ready;
	dont_initialize();
	
	SC_METHOD(debug_print);
	sensitive << fa_done;
	dont_initialize();
}

void FullAdder::inputs_read()
{
	temp_S = A.read() ^ B.read() ^ Cin.read();
	temp_Cout = (A.read() & B.read()) | (Cin.read() & (A.read() ^ B.read()));
	
	temp_G = A.read() & B.read();
	temp_P = A.read() | B.read();	

	fa_ready.notify(SC_ZERO_TIME);
}

void FullAdder::full_addition()
{
	next_trigger(fa_ready);
	
	S.write(temp_S);
	
	Cout->write(temp_Cout);
	Gen->write(temp_G);
	Pro->write(temp_P);

	fa_done.notify(SC_ZERO_TIME);
}

void FullAdder::debug_print()
{
	next_trigger(fa_done);
	
	if (debugMode)
	{
		cout << endl;
		cout << "   FA calling [A = " << A << ", B = " << B;
		cout << ", Cin = " << Cin << ", S = " << S << ", Cou = " << Cout->read();
		cout << ", G = " << Gen->read() << ", P = " << Pro->read() << "]";
		cout << endl << endl;
	}
}