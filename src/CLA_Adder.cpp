#include <systemc.h>
#include "CLA_Adder.hpp"

CLA_Adder::CLA_Adder(sc_module_name cla_name)
:fav("fa_",8), fav_Cin("Cin_",8), fav_Cout("Cout_",8), fav_Gen("Gen_",8), fav_Pro("Pro_",8),
counter_clk(0), enable(false), debugMode(false)
{
	SC_HAS_PROCESS(CLA_Adder);

	SC_METHOD(add_enable);
	sensitive << in_A << in_B;
	dont_initialize();

	SC_THREAD(inner_loop);
	sensitive << clk;
	
	SC_METHOD(read_inputs);
	sensitive << start_read;
	dont_initialize();

	SC_METHOD(carry_update);
	sensitive << carry_prop;
	dont_initialize();

	SC_METHOD(elaborate_result);
	sensitive << read_event;
	dont_initialize();

	SC_METHOD(write_result);
	sensitive << write_event;
	dont_initialize();

	SC_METHOD(report);
	sensitive << clk;
	dont_initialize();

	for (int j=0; j<8; j++)
	{
		fav[j].A(operand_A[j]);
		fav[j].B(operand_B[j]);
		fav[j].S(result[j]);
		fav[j].Cin(fav_Cin[j]);
		fav[j].Cout(fav_Cout[j]);
		fav[j].Gen(fav_Gen[j]);
		fav[j].Pro(fav_Pro[j]);
	}
}

void CLA_Adder::add_enable()
{
	if (!(enable)) {
		counter_clk = 0;
		enable = true;
		if (debugMode) {
			cout << "turn on the adder" << endl;
		}
	}
}

void CLA_Adder::inner_loop()
{
	while(true) {
		wait();
		if (enable) {
	
		if (debugMode) {
			cout << "phase " << counter_clk << " of 4" << endl;
		}
		
		switch (counter_clk) {
			case 0 : {
				start_read.notify();
				break;
			}
			case 1 : {
				read_event.notify();
				carry_prop.notify(SC_ZERO_TIME);
				break;
			}
			case 2 : {
				write_event.notify();
				break;	
			}
			case 3 : {
				enable = false;
				break;
			}
			default : break;
			}
		}
		counter_clk++;
	}
}

void CLA_Adder::read_inputs()
{
	for (int i=0; i<8; i++) {
		operand_A[i] = in_A[i]->read();
		operand_B[i] = in_B[i]->read();
	}

	carry = in_carry->read();
	if (debugMode) {
		cout << "read inputs completed ";
		cout << "(carry is " << in_carry->read() << ")" << endl;
	}
}

void CLA_Adder::carry_update()
{
	if (debugMode) {
	cout << carry << ", ";
	cout << "updating carry inputs (G : ";
	for (int j=0; j<8; j++) {
		cout << fav[j].Gen->read();
	}
	cout << ")(P : ";
	for (int j=0; j<8; j++) {
		cout << fav[j].Pro->read();
	}
	cout << ")";
		cout << "(G1 : ";
		cout << G1 << ")(G0 : " << G0;
		cout << ")(P1 : " << P1 << ")(P0 : " << P0;
		cout << ")" << endl;
	}

	// bit ordering is big endian (carry_in '0' is the most significant)
	fav_Cin[7] = carry;
	fav_Cin[6] = fav[7].Gen->read() | (fav[7].Pro->read() & carry);
	fav_Cin[5] = fav[6].Gen->read() | (fav[6].Pro->read() & fav[7].Gen->read()) |
			(fav[6].Pro->read() & fav[7].Pro->read() & carry);
	fav_Cin[4] = fav[5].Gen->read() | (fav[5].Pro->read() & fav[6].Gen->read()) |
			(fav[5].Pro->read() & fav[6].Pro->read() & fav[7].Gen->read()) |
			(fav[5].Pro->read() & fav[6].Pro->read() & fav[7].Pro->read() & carry);
	
	fav_Cin[3] = G0 | (P0 & carry) ;
	
	fav_Cin[2] = fav[3].Gen->read() | (fav[3].Pro->read() & (G0 | (P0 & carry)));
	fav_Cin[1] = fav[2].Gen->read() | (fav[2].Pro->read() & fav[3].Gen->read()) |
			(fav[2].Pro->read() & fav[3].Pro->read() & (G0 | (P0 & carry)));
	fav_Cin[0] = fav[1].Gen->read() | (fav[1].Pro->read() & fav[2].Gen->read()) |
			(fav[1].Pro->read() & fav[2].Pro->read() & fav[3].Gen->read()) |
			(fav[1].Pro->read() & fav[2].Pro->read() & fav[3].Pro->read() & (G0 | (P0 & carry)));
}

void CLA_Adder::elaborate_result()
{
	G1 = fav[0].Gen->read() | 
		(fav[1].Gen->read() & fav[0].Pro->read()) |
		(fav[2].Gen->read() & fav[1].Pro->read() & fav[0].Pro->read()) |
		(fav[3].Gen->read() & fav[2].Pro->read() & fav[1].Pro->read() & fav[0].Pro->read());
	G0 = fav[4].Gen->read() | 
		(fav[5].Gen->read() & fav[4].Pro->read()) |
		(fav[6].Gen->read() & fav[5].Pro->read() & fav[4].Pro->read()) |
		(fav[7].Gen->read() & fav[6].Pro->read() & fav[5].Pro->read() & fav[4].Pro->read());

	P0 = fav[4].Pro->read() & fav[5].Pro->read() & fav[6].Pro->read() & fav[7].Pro->read();
	P1 = fav[0].Pro->read() & fav[1].Pro->read() & fav[2].Pro->read() & fav[3].Pro->read();
	
	if (debugMode) {
		cout << "elaboration is done (G1 : ";
		cout << G1 << ")(G0 : " << G0;
		cout << ")(P1 : " << P1 << ")(P0 : " << P0;
		cout << ")" << endl;
	}
}

void CLA_Adder::write_result()
{
	out_carry->write(G1 | (P1 & G0) | (P1 & P0 & carry));
	for (int j=0; j<8; j++) {
		out_S[j]->write(result[j].read());
	}
	if (debugMode) {
		cout << "result has been written [S : ";
		for (int j=0; j<8; j++) {
			cout << result[j].read();
		}
		cout << "]" << endl;
	}
}

void CLA_Adder::report()
{
	if (enable && debugMode && (counter_clk == 3)) {
	cout << "TOTAL ELABORATION DURING THIS STAGE:" << endl;
	cout << "A     <- \"";
	for (int i=0; i<8; i++) {
		cout << in_A[i]->read();
	}
	cout << "\"" << endl;
	cout << "B     <- \"";
	for (int i=0; i<8; i++) {
		cout << in_B[i]->read();
	}
	cout << "\"" << endl;
	cout << "C_in  <- \"";
	for (int i=0; i<8; i++) {
		cout << fav_Cin[i];
	}
	cout << "\"" << endl;
	cout << "C_out <- \"";
	for (int i=0; i<8; i++) {
		cout << fav_Cout[i];
	}
	cout << "\"" << endl;
	cout << "Gen   <- \"";
	for (int i=0; i<8; i++) {
		cout << fav_Gen[i];
	}
	cout << "\"" << endl;
	cout << "Prop  <- \"";
	for (int i=0; i<8; i++) {
		cout << fav_Pro[i];
	}
	cout << "\"" << endl;
	cout << "S     <- \"";	
	for (int i=0; i<8; i++) {
		cout << result[i].read();
	}
	cout << "\"" << endl;
	}
}