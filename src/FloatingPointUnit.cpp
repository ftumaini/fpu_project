#include <systemc.h>
#include "FloatingPointUnit.hpp"

FloatingPointUnit::FloatingPointUnit(sc_module_name fp_unit_name)
:FPA("floating_point_add_or_sub_module"),
FPM("floating_point_multiplier_module")
{
	SC_HAS_PROCESS(FloatingPointUnit);

	FPA.clk(this->clk);
	FPA.opcode(add_or_sub);
	FPA.in_A(add_in_A);
	FPA.in_B(add_in_B);
	FPA.out_S(add_out_res);
	
	FPM.clk(this->clk);
	FPM.in_A(mul_in_A);
	FPM.in_B(mul_in_B);
	FPM.out_P(mul_out_mul);
		

	SC_METHOD(operandsFetch)
	sensitive << fp_in_A;
	sensitive << fp_in_B;
	sensitive << opcode;
	dont_initialize();

	SC_METHOD(resultWrite)
	sensitive << add_out_res;
	sensitive << mul_out_mul;
	dont_initialize();
}

void FloatingPointUnit::operandsFetch()
{
	if (opcode.read()[1]) {
		if (!(opcode.read()[0])) {
			//cout << "perform a floating point multiplication" << endl;
			mul_in_A.write(fp_in_A.read());
			mul_in_B.write(fp_in_B.read());
		}
	}
	else {
		add_in_A.write(fp_in_A.read());
		add_in_B.write(fp_in_B.read());
	
		if (opcode.read()[0]) {
			//cout << "perform a floating point subtraction" << endl;
			add_or_sub.write(true);
		} else {
			//cout <<  "perform a floating point addition" << endl;
			add_or_sub.write(false);
		}
	} 	
}

void FloatingPointUnit::resultWrite()
{
	if (opcode.read()[1]) {
		if (!(opcode.read()[0])) {
			fp_out.write(mul_out_mul.read());
		}
	}
	else {
		fp_out.write(add_out_res.read());
	}
}